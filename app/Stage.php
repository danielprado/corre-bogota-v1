<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
    protected $table = 'tbl_escenarios';
    protected $primaryKey = 'i_pk_id';
    protected $fillable = ['vc_escenario'];
}
