<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ethnic extends Model
{
    protected $table = 'tbl_etnia';
    protected $primaryKey = 'id';
    protected $fillable = ['etnia'];
    public $timestamps = false;
}
