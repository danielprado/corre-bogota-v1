<?php


namespace App\Transformers;


use App\Ethnic;
use League\Fractal\TransformerAbstract;

class EthnicTransformer extends TransformerAbstract
{
    public function transform(Ethnic $ethnic)
    {
        return [
            'id'    => isset( $ethnic->id ) ? $ethnic->id : null,
            'text'  => isset( $ethnic->etnia ) ? $ethnic->etnia : null
        ];
    }
}