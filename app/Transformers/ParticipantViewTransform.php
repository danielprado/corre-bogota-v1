<?php


namespace App\Transformers;

use App\Attendance;
use App\ParticipantView;
use App\Sessions;
use App\Stage;
use App\User;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class ParticipantViewTransform extends TransformerAbstract
{
    public function transform(ParticipantView $participant)
    {
        return [
            'id'                    =>  isset( $participant->id ) ? (int) $participant->id : null,
            'document_type'         =>  isset( $participant->document_type_id ) ? (int) $participant->document_type_id : null,
            'document_type_name'    =>  isset( $participant->document_type_name ) ? $participant->document_type_name : null,
            'document'              =>  isset( $participant->document ) ? $participant->document : null,
            'name'                  =>  isset( $participant->name ) ? $this->toUpper( $participant->name  ) : null,
            'lastname'              =>  isset( $participant->lastname ) ? $this->toUpper( $participant->lastname ) : null,
            'birthday'              =>  isset( $participant->birthday ) ? $participant->birthday : null,
            'age'                   =>  isset( $participant->birthday ) ? Carbon::now()->diffInYears( Carbon::parse( $participant->birthday ) ) : null,
            'email'                 =>  isset( $participant->email ) ? $this->toLower( $participant->email ) : null,
            'sex'                   =>  isset( $participant->sex_id ) ? (int) $participant->sex_id : null,
            'sex_name'              =>  isset( $participant->sex_name ) ? $participant->sex_name : null,
            'gender'                =>  isset( $participant->gender_id ) ? (int) $participant->gender_id : null,
            'gender_name'           =>  isset( $participant->gender_name ) ? $participant->gender_name : null,
            'ethnic_group'          =>  isset( $participant->ethnic_group_id ) ? (int) $participant->ethnic_group_id : null,
            'ethnic_group_name'     =>  isset( $participant->ethnic_group_name ) ? $participant->ethnic_group_name : null,
            'social_group'          =>  isset( $participant->social_group_id ) ? (int) $participant->social_group_id : null,
            'social_group_name'     =>  isset( $participant->social_group_name ) ? $participant->social_group_name : null,
            'situation'             =>  isset( $participant->situation_id ) ? (int) $participant->situation_id : null,
            'situation_name'        =>  isset( $participant->situation_name ) ? $participant->situation_name : null,
            'eps'                   =>  isset( $participant->eps ) ? $this->toUpper( $participant->eps ) : null,
            'residence_location'    =>  isset( $participant->location_id ) ? (int) $participant->location_id : null,
            'location_name'         =>  isset( $participant->location_name ) ? $participant->location_name : null,
            'phone_or_mobile'       =>  isset( $participant->phone ) ? $participant->phone : null,
            'stage'                 =>  isset( $participant->id ) ? $this->getStage( $participant->id ) : null,
            'trainer'               =>  isset( $participant->id ) ? $this->getTrainer( $participant->id ) : null,
            'created_at'            =>  isset( $participant->created_at ) ? $participant->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'            =>  isset( $participant->updated_at ) ? $participant->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }

    public function toUpper( $string = null )
    {
        return mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_UPPER, 'UTF-8');
    }

    public function toLower( $string = null )
    {
        return mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_LOWER, 'UTF-8');
    }

    public function getStage( $id = null )
    {
        if ( $id ) {
            $session = Attendance::query()->where('participant_id', $id)->first(['session_id']);
            if ( isset( $session->session_id ) )  {
                $stage   = Sessions::query()->where('id',  $session->session_id)->first(['park']);
                if ( isset( $stage->park ) ) {
                    $stage_name = Stage::query()->where('i_pk_id', $stage->park)->first(['vc_escenario']);

                    return isset( $stage_name->vc_escenario ) ? $stage_name->vc_escenario : null;
                }
            }
        }

        return null;
    }

    public function getTrainer( $id = null )
    {
        if ( $id ) {
            $session = Attendance::query()->where('participant_id', $id)->first(['session_id']);
            if ( isset( $session->session_id ) )  {
                $stage   = Sessions::query()->where('id',  $session->session_id)->first(['user_id']);
                if ( isset( $stage->user_id ) ) {
                    $name = User::query()->where('Id_Persona', $stage->user_id)->first();

                    return isset( $name->full_name ) ? $name->full_name : null;
                }
            }
        }

        return null;
    }
}