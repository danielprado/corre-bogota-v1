<?php


namespace App\Transformers;


use App\Stage;
use League\Fractal\TransformerAbstract;

class StageTransformer extends TransformerAbstract
{
    public function transform( Stage $stage )
    {
        return [
            'id'    =>  isset( $stage->i_pk_id ) ? $stage->i_pk_id : null,
            'text'  =>  isset( $stage->vc_escenario ) ? $stage->vc_escenario : null,
        ];
    }
}