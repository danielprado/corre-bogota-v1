<?php


namespace App\Transformers;


use App\Situation;
use League\Fractal\TransformerAbstract;

class SituationTransformer extends TransformerAbstract
{
    public function transform(Situation $situation)
    {
        return [
            'id'                =>  isset( $situation->id ) ? $situation->id : null,
            'text'              =>  isset( $situation->situacion ) ? $situation->situacion : null,
            'population_id'     =>  isset( $situation->population_id ) ? (int) $situation->population_id : null,
        ];
    }
}