<?php


namespace App\Transformers;


use App\Location;
use League\Fractal\TransformerAbstract;

class LocationTransformer extends TransformerAbstract
{
    public function transform(Location $location)
    {
        return [
            'id'      =>  isset( $location->id_localidad ) ?     $location->id_localidad : 0,
            'text'    =>  isset( $location->localidad ) ? $location->localidad : null
        ];
    }
}