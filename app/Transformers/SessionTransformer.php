<?php


namespace App\Transformers;


use App\Attendance;
use App\Evidence;
use App\Participant;
use App\Sessions;
use App\Stage;
use App\User;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class SessionTransformer extends TransformerAbstract
{
    public function transform( Sessions $sessions )
    {



        $part = isset( $sessions->schedule ) ? $sessions->schedule : null;
        $initial_hour = isset( $sessions->initial_hour ) ? $sessions->initial_hour : null;
        $final_hour = isset( $sessions->final_hour ) ? $sessions->final_hour : null;

        //$initial_hour = isset( $initial_hour ) ? $initial_hour : ($part == "AM") ? '08:00:00' : '14:00:00';
        //$final_hour = isset( $final_hour )     ? $final_hour   : ($part == "AM") ? '11:00:00' : '16:00:00';

        return [
            'id'        =>  isset( $sessions->id ) ? (int) $sessions->id : null,
            'park'      =>  isset( $sessions->park ) ? (int) $sessions->park : null,
            'park_name' =>  isset( $sessions->park ) ? $this->getNamePark( $sessions->park ) : null,
            'title'     =>  isset( $sessions->park ) ? $this->getNamePark( $sessions->park ) : null,
            'schedule'  =>  isset( $sessions->schedule ) ? $sessions->schedule : null,
            'day'       =>  isset( $sessions->day ) ? $sessions->day : null,
            'start'     =>  isset( $sessions->day  ) ? "$sessions->day $initial_hour" : null,
            'end'       =>  isset( $sessions->day  ) ? "$sessions->day $final_hour" : null,
            'initial_time' => substr( $initial_hour, 0,5 ),
            'end_time' => substr( $final_hour, 0,5 ),
            'allDay'    =>  false,
            'className' => isset( $sessions->day ) ? $this->getClassName( $sessions->day ) : null,
            'user_id'   =>  isset( $sessions->user_id ) ? (int) $sessions->user_id : null,
            'user_name'    =>  isset( $sessions->user_id ) ? $this->getUserName( $sessions->user_id ) : null,
            'participants' =>  isset( $sessions->id ) ? $this->getAttendances( $sessions->id ) : [],
            'images'       =>  isset( $sessions->id ) ? $this->getImages( $sessions->id ) : [],
            'created_at'   =>  isset( $sessions->created_at ) ? $sessions->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'   =>  isset( $sessions->updated_at ) ? $sessions->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }

    public function getClassName( $date = null )
    {
        if ( isset( $date ) ) {
            $date = Carbon::parse( $date );

            if ( Carbon::now()->greaterThan( $date ) && Carbon::now()->format('Y-m-d') != $date->format('Y-m-d') ) {
                return 'event-azure';
            }

            if ( Carbon::now()->lessThan( $date ) && Carbon::now()->format('Y-m-d') != $date->format('Y-m-d') ) {
                return 'event-green';
            }

            if ( Carbon::now()->format('Y-m-d') == $date->format('Y-m-d') ) {
                return 'event-orange';
            }
        }

        return 'event-red';
    }

    public function getImages( $id )
    {
        $images = Evidence::query()->where('session_id', $id)->get();
        $arr = [];
        if ( $images ) {
            foreach ( $images as $image) {
                $arr[] = [
                    'id'            =>  isset( $image->id ) ? $image->id : null,
                    'filename'      =>  isset( $image->filename ) ? $image->filename : null,
                    'filetype'      =>  isset( $image->filetype ) ? $image->filetype : null,
                    'path'          =>  isset( $image->path )
                        ? file_exists( public_path('/storage/photo').'/'.$image->path ) ? asset('public/storage/photo').'/'.$image->path : asset('public/img/lock.jpg')
                        : null,
                    'description'   =>  isset( $image->description ) ? $image->description : null,
                    'user_id'       =>  isset( $image->user_id ) ? $image->user_id : null,
                    'session_id'    =>  isset( $image->session_id ) ? $image->session_id : null,
                    'created_at'    =>  isset( $image->created_at ) ? $image->created_at->format('Y-m-d H:i:s') : null,
                    'updated_at'    =>  isset( $image->updated_at ) ? $image->updated_at->format('Y-m-d H:i:s') : null,
                ];
            }
        }

        return $arr;
    }

    public function getNamePark( $id )
    {
        $park = Stage::query()->where('i_pk_id', $id)->first();
        return isset( $park->vc_escenario ) ? $park->vc_escenario : null;
    }

    public function getUserName( $id )
    {
        $user = User::query()->where('Id_Persona', $id)->first();
        return isset( $user->full_name ) ? $user->full_name : null;
    }

    public function getParticipantName( $id )
    {
        $user = Participant::query()->where('id', $id)->first();
        return isset( $user->name, $user->lastname ) ? "{$user->name} {$user->lastname}" : null;
    }

    public function getAttendances( $id )
    {
        $attendances = Attendance::query()->where('session_id', $id)->get();
        $arr = [];
        if ( $attendances ) {
            foreach ($attendances as $attendance) {
                $arr[] = [
                    'id'                =>  isset( $attendance->id ) ? (int) $attendance->id : null,
                    'participant_id'    =>  isset( $attendance->participant_id ) ? (int) $attendance->participant_id : null,
                    'participant'       =>  isset( $attendance->participant_id ) ? $this->getParticipantName( $attendance->participant_id ) : null,
                    'attendance'        =>  isset( $attendance->attendance ) ? (bool) $attendance->attendance : null,
                ];
            }
        }

        return $arr;
    }
}