<?php


namespace App\Transformers;


use App\Gender;
use League\Fractal\TransformerAbstract;

class GenderTransformer extends TransformerAbstract
{
    public function transform( Gender $gender )
    {
        return [
            'id'    => isset( $gender->id ) ? $gender->id : null,
            'text'  => isset( $gender->identidad )  ? $gender->identidad: null
        ];
    }
}