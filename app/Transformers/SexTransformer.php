<?php


namespace App\Transformers;


use App\Sex;
use League\Fractal\TransformerAbstract;

class SexTransformer extends TransformerAbstract
{
    public function transform( Sex $sex )
    {
        return [
            'id'    => isset( $sex->id ) ? $sex->id : null,
            'text'  => isset( $sex->name ) ? $sex->name : null
        ];
    }
}