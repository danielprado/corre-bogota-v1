<?php


namespace App\Transformers;


use App\SocialGroup;
use League\Fractal\TransformerAbstract;

class SocialGroupTransformer extends TransformerAbstract
{
    public function transform(SocialGroup $group)
    {
        return [
            'id'    =>  isset( $group->id ) ? $group->id : null,
            'text'  =>  isset( $group->poblacion ) ? $group->poblacion : null,
            'has_select' =>  isset( $group->has_select ) ? (bool) $group->has_select : null,
        ];
    }
}