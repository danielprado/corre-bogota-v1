<?php


namespace App\Transformers;


use App\Evidence;
use League\Fractal\TransformerAbstract;

class EvidenceTransformer extends TransformerAbstract
{
    public function transform( Evidence $evidence )
    {
        return [
            'id'            => isset( $evidence->id ) ? $evidence->id : null,
            'filename'      => isset( $evidence->filename ) ? $evidence->filename : null,
            'filetype'      => isset( $evidence->filetype ) ? $evidence->filetype : null,
            'path'          => isset( $evidence->path ) ? asset("public/storage/{$evidence->filetype}/".$evidence->path) : null,
            'month'         => isset( $evidence->month ) ? $evidence->month : null,
            'description'   => isset( $evidence->description ) ? $evidence->description : null,
            'user_id'       => isset( $evidence->user_id ) ? $evidence->user_id : null,
            'session_id'    => isset( $evidence->session_id ) ? $evidence->session_id : null,
        ];
    }
}