<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Idrd\Usuarios\Repo\Persona as MPersona;

class Persona extends MPersona implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    protected $appends = ['full_name', 'part_name'];

    public function getFullNameAttribute()
    {
        $n1 = isset( $this->Primer_Nombre ) ?     $this->Primer_Nombre : null;
        $n2 = isset( $this->Segundo_Nombre )    ? $this->Segundo_Nombre : null;
        $n3 = isset( $this->Primer_Apellido )   ? $this->Primer_Apellido : null;
        $n4 = isset( $this->Segundo_Apellido )  ? $this->Segundo_Apellido : null;

        $full_name = trim( $n1 ).' '.trim( $n2 ).' '.trim( $n3 ).' '.trim( $n4 );

        return trim( $full_name );
    }

    public function getPartNameAttribute()
    {
        $n1 = isset( $this->Primer_Nombre ) ?     $this->Primer_Nombre : null;
        $n3 = isset( $this->Primer_Apellido )   ? $this->Primer_Apellido : null;

        $full_name = trim( $n1 ).' '.trim( $n3 );

        return trim( $full_name );
    }
}
