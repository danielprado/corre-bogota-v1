<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sessions extends Model
{
    protected $table = 'tbl_sessions';
    protected $primaryKey = 'id';
    protected $fillable = [
        'park',
        'schedule',
        'day',
        'initial_hour',
        'final_hour',
        'user_id'
    ];

    public function attendance()
    {
        return $this->belongsToMany( Attendance::class, 'tbl_attendance', 'session_id', 'participant_id' )->withPivot('id');
    }

    public function participants()
    {
        return $this->hasManyThrough(Attendance::class, ParticipantView::class, 'id', 'participant_id', 'id');
    }
}
