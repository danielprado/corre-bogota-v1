<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Situation extends Model
{
    protected $table = 'tbl_situacion';
    protected $primaryKey = 'id';
    protected $fillable = ['situation', 'population_id'];
    public $timestamps = false;
}
