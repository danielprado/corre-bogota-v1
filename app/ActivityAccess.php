<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config as Config;

class ActivityAccess extends Model
{
    protected $table = 'actividad_acceso';
    protected $primaryKey = 'Id_Persona';
    protected $fillable = ['Id_Actividad', 'Estado'];
    protected $connection = '';
    public $timestamps = false;

    public function __construct()
    {
        $this->connection = config('usuarios.conexion');
        $this->table = config('database.connections.'.$this->connection.'.database').'.'.$this->table;
    }
}
