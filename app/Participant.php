<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    protected $table = 'tbl_participants';
    protected $primaryKey = 'id';
    protected $fillable = [
        'document_type_id',
        'document',
        'name',
        'lastname',
        'birthday',
        'email',
        'sex_id',
        'gender_id',
        'ethnic_group_id',
        'social_group_id',
        'situation_id',
        'eps',
        'location_id',
        'phone_or_mobile',
    ];
}
