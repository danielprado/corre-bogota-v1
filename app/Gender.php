<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    protected $table = 'tbl_identidad';
    protected $primaryKey = 'id';
    protected $fillable = ['identidad'];
    public $timestamps = false;
}
