<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evidence extends Model
{
    const AGREEMENT = 'agreement';
    const PHOTO = 'photo';
    const WORK_PLAN = 'work_plan';
    const TRAINING_PLAN = 'training_plan';

    protected $table = 'tbl_evidencias';
    protected $primaryKey = 'id';
    protected $fillable = [
        'filename',
        'filetype',
        'path',
        'description',
        'user_id',
        'session_id',
    ];
}
