<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'localidad';
    protected $primaryKey = 'id_localidad';
    protected $fillable = ['localidad'];
}
