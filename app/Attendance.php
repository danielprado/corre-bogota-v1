<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $table = 'tbl_attendance';
    protected $primaryKey = 'id';
    protected $fillable = [
        'session_id',
        'participant_id',
        'attendance'
    ];
    public $timestamps = false;


    public function sessions()
    {
        return $this->belongsTo( Sessions::class, 'session_id' );
    }

}
