<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sex extends Model
{
    protected $table = 'tbl_sex';
    protected $primaryKey = 'id';
    protected $fillable = ['name'];
    public $timestamps = false;
}
