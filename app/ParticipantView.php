<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParticipantView extends Model
{
    protected $table = "tbl_participants_view";


    public function attendance()
    {
        return $this->hasMany( Attendance::class, 'participant_id', 'id' );
    }

}
