<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialGroup extends Model
{
    protected $table = 'tbl_poblacion';
    protected $primaryKey = 'id';
    protected $fillable = ['poblacion'];
    public $timestamps = false;
}
