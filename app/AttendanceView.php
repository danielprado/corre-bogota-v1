<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendanceView extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tbl_view_attendance';


    public function attendance()
    {
        return $this->hasOne( Attendance::class, 'attendance_id', 'id' );
    }

}
