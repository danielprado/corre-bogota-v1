<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreSessionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return isset( $_SESSION['Usuario']['Permisos']['access_platform'] )
                    && $_SESSION['Usuario']['Permisos']['access_platform'] == 1
                        ? true
                        : false;
    }

    public function forbiddenResponse()
    {
        return request()->wantsJson()
            ? response()->json([
                'message' =>  trans('validation.handler.unauthorized'),
                'code'  =>  403
            ], 403)
            : view('errors.403');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'park'          =>  'required|integer',
            'day'           =>  'required|date|date_format:Y-m-d',
            'participants'  =>  'required|array',
            'participants.*'=>  'required|integer'
        ];
    }
}
