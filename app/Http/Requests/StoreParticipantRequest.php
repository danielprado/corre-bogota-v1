<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreParticipantRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return isset( $_SESSION['Usuario']['Permisos']['access_platform'] )
        && $_SESSION['Usuario']['Permisos']['access_platform'] == 1
            ? true
            : false;
    }

    public function forbiddenResponse()
    {
        return request()->wantsJson()
            ? response()->json([
                'message' =>  trans('validation.handler.unauthorized'),
                'code'  =>  403
            ], 403)
            : view('errors.403');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'document_type' =>  'required|integer',
            'document'  =>  'required|string|min:3,max:12',
            'name'  =>  'required|string|min:3,max:70',
            'lastname'  =>  'required|string|min:3,max:70',
            'birthday'  =>  'required|date|date_format:Y-m-d',
            'email' =>  'email|min:6,max:100',
            'sex'   =>  'required|integer',
            'gender'    =>  'required|integer',
            'ethnic_group'  =>  'required|integer',
            'social_group'  =>  'required|integer',
            'situation' =>  'integer',
            'eps'   =>  'required|string|max:70',
            'residence_location'    =>  'required|integer',
            'phone_or_mobile'   =>  'required|string|min:7,max:10',
        ];
    }
}
