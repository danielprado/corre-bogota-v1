<?php

namespace App\Http\Controllers\Attendance;

use App\Attendance;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class AttendanceController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param Requests\UpdateAttendanceRequest $request
     * @param Attendance $attendance
     * @return Response
     * @throws \Exception
     */
    public function update(Requests\UpdateAttendanceRequest $request, Attendance $attendance)
    {
        $attendance->attendance = $request->get('attendance');
        if ( (bool) $request->get('attendance') ) {
            $attendance->save();
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  => 200
            ], 200);
        } else {
            $attendance->delete();
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  => 200
            ], 200);
        }
    }
}
