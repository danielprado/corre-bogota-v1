<?php

namespace App\Http\Controllers\Stage;

use App\Stage;
use App\Http\Controllers\Controller;
use App\Transformers\StageTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class StageController extends Controller
{
    public function index()
    {
        $resource = Stage::all();
        $resource = new Collection($resource, new StageTransformer());
        $manager = new Manager();
        $resource = $manager->createData($resource);
        return response()->json($resource->toArray(), 200);
    }
}
