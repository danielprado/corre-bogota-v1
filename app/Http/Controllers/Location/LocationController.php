<?php

namespace App\Http\Controllers\Location;


use App\Location;
use App\Transformers\LocationTransformer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $data = Location::all();
        $resource = new Collection($data, new LocationTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);

        return response()->json($rootScope->toArray(), 200);
    }

}
