<?php

namespace App\Http\Controllers\Report;

use App\Attendance;
use App\AttendanceView;
use App\ParticipantView;
use App\Persona;
use App\Sessions;
use App\Stage;
use Carbon\Carbon;
use FPDI;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Style_Border;

class ReportController extends Controller
{
    public function example(Requests\AttendanceReportRequest $request)
    {
        //$date = Carbon::parse( "{$request->get('year')}-{$request->get('month')}-25" );
        $start = Carbon::parse( $request->get('start_day') )->format('Y-m-d');
        $end   = Carbon::parse( $request->get('end_day') )->format('Y-m-d');



        $sessions = Sessions::query()->where('user_id', $_SESSION['Id_funcionario'])->where('schedule', $request->get('schedule'))->where('park', $request->get('park') )->whereBetween('day', [$start, $end])->orderBy('day')->get();
        $days = [];

        foreach ($sessions->pluck('day')->toArray() as $day) {
            $days[ (int) Carbon::parse( $day )->format('d') ] = (int) Carbon::parse( $day )->format('d');
        }

        //$attendance = Attendance::whereIn('session_id', $sessions->pluck('id')->toArray() )->get();
        $participants = ParticipantView::has('attendance')->with([
            'attendance' => function($q) use( $request, $start, $end ) {
                return $q->whereHas('sessions', function ($query) use( $request, $start, $end ) {
                    //->where('user_id', $_SESSION['Id_funcionario'])
                    return $query->where('user_id', $_SESSION['Id_funcionario'])->where('schedule', $request->get('schedule'))->where('park', $request->get('park') )->whereBetween('day', [$start, $end]);
                })->with('sessions');
            }
        ])->get();

        $arr = [];
        $i = 1;
        $row = 13;
        $c = 0;

        if ( $participants ) {
            foreach ( $participants as $participant ) {
                if ( count( $participant->attendance ) > 0 ) {

                    $arr[$c] = [
                        'n'             =>  $i,
                        'name'          =>  isset( $participant->name, $participant->lastname ) ? "{$participant->name} {$participant->lastname}" : null,
                        'document_type' =>  isset( $participant->document_type_name ) ? $participant->document_type_name : null,
                        'document'      =>  isset( $participant->document ) ? $participant->document : null,
                        'age'           =>  isset( $participant->birthday ) ? Carbon::now()->diffInYears( Carbon::parse( $participant->birthday ) ) : null,
                        'location_name' =>  isset( $participant->location_name ) ? $participant->location_name : null,
                        'phone_or_mobile'       =>  isset( $participant->phone ) ? $participant->phone : null,
                        'eps'                   =>  isset( $participant->eps ) ? $participant->eps : null,
                        'male'                  =>  isset( $participant->sex_name ) &&  $participant->sex_name == 'MUJER' ? 'X' : null,
                        'female'                =>  isset( $participant->sex_name ) &&  $participant->sex_name == 'HOMBRE' ? 'X' : null,
                        'fem'                =>  isset( $participant->sex_name ) &&  $participant->gender_name == 'FEMENINO' ? 'X' : null,
                        'mal'                =>  isset( $participant->sex_name ) &&  $participant->gender_name == 'MASCULINO' ? 'X' : null,
                        'trans'              =>  isset( $participant->sex_name ) &&  $participant->gender_name == 'TRANSGENERO' ? 'X' : null,
                        'afro'     =>  isset( $participant->ethnic_group_name ) && $participant->ethnic_group_name == 'AFRO' ? 'X' : null,
                        'raiz'     =>  isset( $participant->ethnic_group_name ) && $participant->ethnic_group_name == 'RAIZAL' ? 'X' : null,
                        'indi'     =>  isset( $participant->ethnic_group_name ) && $participant->ethnic_group_name == 'INDÍGENA' ? 'X' : null,
                        'git'      =>  isset( $participant->ethnic_group_name ) && $participant->ethnic_group_name == 'GITANO' ? 'X' : null,
                        'mest'     =>  isset( $participant->ethnic_group_name ) && $participant->ethnic_group_name == 'MESTIZO' ? 'X' : null,
                        'camp'    => isset( $participant->social_group_id ) && (int) $participant->social_group_id == 1 ? 'X' : null,
                        'fis'    => isset( $participant->situation_id ) && (int) $participant->situation_id == 1 ? 'X' : null,
                        'vis'    => isset( $participant->situation_id ) && (int) $participant->situation_id == 2 ? 'X' : null,
                        'aud'    => isset( $participant->situation_id ) && (int) $participant->situation_id == 3 ? 'X' : null,
                        'cog'    => isset( $participant->situation_id ) && (int) $participant->situation_id == 4 ? 'X' : null,
                        'art'     => isset( $participant->social_group_id ) && (int) $participant->social_group_id == 2 ? 'X' : null,
                        'street'  => isset( $participant->social_group_id ) && (int) $participant->social_group_id == 3 ? 'X' : null,
                        'victim'  => isset( $participant->social_group_id ) && (int) $participant->social_group_id == 4 ? 'X' : null,
                        'ext'     => isset( $participant->social_group_id ) && (int) $participant->social_group_id == 5 ? 'X' : null,
                        'general' => isset( $participant->social_group_id ) && (int) $participant->social_group_id == 6 ? 'X' : null,
                        'les'    => isset( $participant->situation_id ) && (int) $participant->situation_id == 6 ? 'X' : null,
                        'gay'    => isset( $participant->situation_id ) && (int) $participant->situation_id == 7 ? 'X' : null,
                        'bis'    => isset( $participant->situation_id ) && (int) $participant->situation_id == 8 ? 'X' : null,
                        'trn'    => isset( $participant->situation_id ) && (int) $participant->situation_id == 9 ? 'X' : null,
                        'int'    => isset( $participant->situation_id ) && (int) $participant->situation_id == 10 ? 'X' : null,
                    ];

                    $count = 0;
                    $spaces = count( $days );
                    foreach ( $days as $day ) {
                        $arr[$c][$day] = '';
                    }
                    if ( $spaces < 15 ) {
                        $rest = 15 - $spaces;
                        for ( $x = 0; $x < $rest; $x++ ) {
                            $arr[$c]["space_$x"] = '';
                        }
                    }
                    foreach ( $participant->attendance as $value ) {
                        $key = isset( $value->sessions->day ) ? (int) Carbon::parse( $value->sessions->day )->format('d') : null;
                        $value = (isset( $value->attendance ) && $value->attendance == true ) ? $value->attendance : null;
                        if ( $value) {
                            $arr[$c][$key] = 'X';
                            $count++;
                        } else {
                            $arr[$c][$key] = '';
                        }
                    }

                    $arr[$c]['sum'] = $count;
                    $row++;
                    $i++;
                    $c++;
                }
            }
        }

        $park = Stage::find( $request->get('park') );
        $park_name = (isset( $park->vc_escenario )) ? $park->vc_escenario : null;

        $data = $arr;

        Excel::load( public_path('excel/REPORTE_ASISTENCIA_NUEVO.xlsx'), function ($file) use ( $data, $start, $end, $park_name, $request, $days ) {
            $file->sheet(0, function ($sheet) use ( $data, $start, $end , $park_name, $request, $days) {
                $row = 13;

                $sheet->cell("J6", function($cell)  {
                    $cell->setValue( isset(  $_SESSION['auth']->full_name ) ?  $_SESSION['auth']->full_name : '' );
                });
                $sheet->cell("AH10", function($cell) use ($row, $start, $end )  {
                    $cell->setValue( "PERIODO DEL $start AL $end " );
                });

                $sheet->fromArray($days, null, 'AH12', true);

                $sheet->cell("C6", function($cell) use ($park_name )  {
                    $cell->setValue( $park_name );
                });
                $sheet->cell("C7", function($cell) use ( $request )  {
                    $cell->setValue( $request->get('schedule') );
                });


                foreach ( $data as $datum ) {
                    $sheet->row($row, $datum);
                    $sheet->setHeight($row, 60);
                    $row++;
                }

                $sum = $row-1;

                $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                );

                $sheet->getStyle("A13:AX$sum")->applyFromArray($styleArray);
                $f = $sum + 1;
                $sheet->mergeCells("A$f:AX$f");
                $sheet->cell("A$f", function($cell)  {
                    $cell->setValue( "CONVENCIONES:  Tipo de Documento:    Cedula de Ciudadanía: (CC),   Cedula de Extranjería (CE),   Pasaporte   (P)" );
                });
                $f1 = $f + 1;
                $sheet->cells("A$f1:AX$f1", function ($cells) {
                    $cells->setValignment('center');
                    $cells->setAlignment('left');
                    $cells->setFontWeight('bold');
                });
                $sheet->mergeCells("A$f1:AX$f1");
                $sheet->setHeight($f1, 50);
                $sheet->cell("A$f1", function($cell)  {
                    $cell->setValignment('center');
                    $cell->setAlignment('left');
                    $cell->setFontWeight('bold');
                    $cell->setValue( "NOTA: Los arriba firmantes inscritos al programa deportivo, certifican que han sido informados sobre el objetivo, los beneficios y posibles riesgos que pudieran surgir durante el desarrollo de los mismos, cuya responsabilidad es asumida enteramente.  Exonerando al Instituto Distrital de Recreación y Deporte IDRD de responsabilidades que van más allá de la notificación de la EPS o Sisbén. Adicional autorizo al IDRD para el manejo y uso adecuado de datos personales, fotografías y videos dentro del marco al derecho a la información y la autodeterminación informativa o protección de datos personales y publicar en la página web del Instituto Distrital de Recreación y Deporte IDRD y demás medios de comunicación radiales y de televisión lo que se considere necesarios; en el marco de la participación del programa. Con mi firma doy constancia de que leí, entendí y acepto el contenido del presente documento." );
                });

            });
        })->export('xlsx');

    }

    public function attendance(Requests\AttendanceReportRequest $request)
    {
        //$date = Carbon::parse( "{$request->get('year')}-{$request->get('month')}-25" );
        $start = Carbon::parse( $request->get('start_day') )->format('Y-m-d');
        $end   = Carbon::parse( $request->get('end_day') )->format('Y-m-d');



        $sessions = Sessions::query()->where('user_id', $_SESSION['Id_funcionario'])->where('schedule', $request->get('schedule'))->where('park', $request->get('park') )->whereBetween('day', [$start, $end])->orderBy('day')->get();
        $days = [];

        foreach ($sessions->pluck('day')->toArray() as $day) {
            $days[ (int) Carbon::parse( $day )->format('d') ] = (int) Carbon::parse( $day )->format('d');
        }

        //$attendance = Attendance::whereIn('session_id', $sessions->pluck('id')->toArray() )->get();
        $participants = ParticipantView::has('attendance')->with([
            'attendance' => function($q) use( $request, $start, $end ) {
                return $q->whereHas('sessions', function ($query) use( $request, $start, $end ) {
                    //->where('user_id', $_SESSION['Id_funcionario'])
                    return $query->where('user_id', $_SESSION['Id_funcionario'])->where('schedule', $request->get('schedule'))->where('park', $request->get('park') )->whereBetween('day', [$start, $end]);
                })->with('sessions');
            }
        ])->get();

        $arr = [];
        $i = 1;
        $row = 13;
        $c = 0;

        if ( $participants ) {
            foreach ( $participants as $participant ) {
                if ( count( $participant->attendance ) > 0 ) {
                    $arr[$c] = [
                        'n'             =>  $i,
                        'name'          =>  isset( $participant->name, $participant->lastname ) ? "{$participant->name} {$participant->lastname}" : null,
                        'document_type' =>  isset( $participant->document_type_name ) ? $participant->document_type_name : null,
                        'document'      =>  isset( $participant->document ) ? $participant->document : null,
                        'age'           =>  isset( $participant->birthday ) ? Carbon::now()->diffInYears( Carbon::parse( $participant->birthday ) ) : null,
                        'location_name' =>  isset( $participant->location_name ) ? $participant->location_name : null,
                        'phone_or_mobile'       =>  isset( $participant->phone ) ? $participant->phone : null,
                        'eps'                   =>  isset( $participant->eps ) ? $participant->eps : null,
                        'male'                  =>  isset( $participant->sex_name ) &&  $participant->sex_name == 'MUJER' ? 'X' : null,
                        'female'                =>  isset( $participant->sex_name ) &&  $participant->sex_name == 'HOMBRE' ? 'X' : null,
                        'fem'                =>  isset( $participant->sex_name ) &&  $participant->gender_name == 'FEMENINO' ? 'X' : null,
                        'mal'                =>  isset( $participant->sex_name ) &&  $participant->gender_name == 'MASCULINO' ? 'X' : null,
                        'trans'              =>  isset( $participant->sex_name ) &&  $participant->gender_name == 'TRANSGENERO' ? 'X' : null,
                        'afro'     =>  isset( $participant->ethnic_group_name ) && $participant->ethnic_group_name == 'AFRO' ? 'X' : null,
                        'raiz'     =>  isset( $participant->ethnic_group_name ) && $participant->ethnic_group_name == 'RAIZAL' ? 'X' : null,
                        'indi'     =>  isset( $participant->ethnic_group_name ) && $participant->ethnic_group_name == 'INDÍGENA' ? 'X' : null,
                        'git'      =>  isset( $participant->ethnic_group_name ) && $participant->ethnic_group_name == 'GITANO' ? 'X' : null,
                        'mest'     =>  isset( $participant->ethnic_group_name ) && $participant->ethnic_group_name == 'MESTIZO' ? 'X' : null,
                        'camp'    => isset( $participant->social_group_id ) && (int) $participant->social_group_id == 1 ? 'X' : null,
                        'fis'    => isset( $participant->situation_id ) && (int) $participant->situation_id == 1 ? 'X' : null,
                        'vis'    => isset( $participant->situation_id ) && (int) $participant->situation_id == 2 ? 'X' : null,
                        'aud'    => isset( $participant->situation_id ) && (int) $participant->situation_id == 3 ? 'X' : null,
                        'cog'    => isset( $participant->situation_id ) && (int) $participant->situation_id == 4 ? 'X' : null,
                        'art'     => isset( $participant->social_group_id ) && (int) $participant->social_group_id == 2 ? 'X' : null,
                        'street'  => isset( $participant->social_group_id ) && (int) $participant->social_group_id == 3 ? 'X' : null,
                        'victim'  => isset( $participant->social_group_id ) && (int) $participant->social_group_id == 4 ? 'X' : null,
                        'ext'     => isset( $participant->social_group_id ) && (int) $participant->social_group_id == 5 ? 'X' : null,
                        'general' => isset( $participant->social_group_id ) && (int) $participant->social_group_id == 6 ? 'X' : null,
                        'les'    => isset( $participant->situation_id ) && (int) $participant->situation_id == 6 ? 'X' : null,
                        'gay'    => isset( $participant->situation_id ) && (int) $participant->situation_id == 7 ? 'X' : null,
                        'bis'    => isset( $participant->situation_id ) && (int) $participant->situation_id == 8 ? 'X' : null,
                        'trn'    => isset( $participant->situation_id ) && (int) $participant->situation_id == 9 ? 'X' : null,
                        'int'    => isset( $participant->situation_id ) && (int) $participant->situation_id == 10 ? 'X' : null,
                    ];

                    $count = 0;
                    $spaces = count( $days );
                    foreach ( $days as $day ) {
                        $arr[$c][$day] = '';
                    }
                    if ( $spaces < 15 ) {
                        $rest = 15 - $spaces;
                        for ( $x = 0; $x < $rest; $x++ ) {
                            $arr[$c]["space_$x"] = '';
                        }
                    }
                    foreach ( $participant->attendance as $value ) {
                        $key = isset( $value->sessions->day ) ? (int) Carbon::parse( $value->sessions->day )->format('d') : null;
                        $value = (isset( $value->attendance ) && $value->attendance == true ) ? $value->attendance : null;
                        if ( $value) {
                            $arr[$c][$key] = 'A';
                            $count++;
                        } else {
                            $arr[$c][$key] = 'F';
                        }
                    }

                    if ( $i == 15 ) {
                        $i = 0;
                    }

                    $arr[$c]['sum'] = $count;
                    $row++;
                    $i++;
                    $c++;
                }
            }
        }

        $park = Stage::find( $request->get('park') );
        $park_name = (isset( $park->vc_escenario )) ? $park->vc_escenario : null;

        $data = $arr;

        Excel::load( public_path('excel/REPORTE_ASISTENCIA_PARTICIPANTES.xlsx'), function ($file) use ( $data, $start, $end, $park_name, $request, $days ) {
            $file->sheet(0, function ($sheet) use ( $data, $start, $end , $park_name, $request, $days) {
                $row = 13;
                $x = 1;

                $sheet->cell("M6", function($cell)  {
                    $cell->setValue( isset(  $_SESSION['auth']->full_name ) ?  $_SESSION['auth']->full_name : '' );
                });
                $sheet->cell("AJ10", function($cell) use ($row, $start, $end )  {
                    $cell->setValue( "PERIODO DEL $start AL $end " );
                });

                $sheet->fromArray($days, null, 'AH12', true);

                $sheet->cell("C7", function($cell) use ($park_name )  {
                    $cell->setValue( $park_name );
                });

                $sheet->cell("M7", function($cell) use ($park_name )  {
                    //$cell->setValue( $park_name );
                });
                $sheet->cell("AJ7", function($cell) use ( $request )  {
                    //$cell->setValue( $request->get('schedule') );
                });

                $pages = count( $data );

                foreach ( $data as $datum ) {
                    $pages--;
                    $sheet->row($row, $datum);
                    $sheet->setHeight($row, 71);
                    if ( $x == 15 || $pages == 0) {
                        $x = 0;
                        $row = $row + 1;
                        $f = $row;
                        $sheet->mergeCells("A$f:AX$f");
                        $sheet->setHeight($row, 16);
                        $sheet->cell("A$f", function($cell)  {
                            $cell->setValue( "CONVENCIONES:  Tipo de Documento:    Cedula de Ciudadanía: (CC),   Cedula de Extranjería (CE),   Pasaporte   (P)" );
                        });
                        $f1 = $f + 1;
                        $sheet->cells("A$f1:AX$f1", function ($cells) {
                            $cells->setValignment('center');
                            $cells->setAlignment('left');
                            $cells->setFontWeight('bold');
                        });
                        $sheet->mergeCells("A$f1:AX$f1");
                        $sheet->setHeight($f1, 56);
                        $sheet->cell("A$f1", function($cell)  {
                            $cell->setValignment('center');
                            $cell->setAlignment('left');
                            $cell->setFontWeight('bold');
                            $cell->setValue( "NOTA: Los arriba firmantes inscritos al programa deportivo, certifican que han sido informados sobre el objetivo, los beneficios y posibles riesgos que pudieran surgir durante el desarrollo de los mismos, cuya responsabilidad es asumida enteramente.  Exonerando al Instituto Distrital de Recreación y Deporte IDRD de responsabilidades que van más allá de la notificación de la EPS o Sisbén. Adicional autorizo al IDRD para el manejo y uso adecuado de datos personales, fotografías y videos dentro del marco al derecho a la información y la autodeterminación informativa o protección de datos personales y publicar en la página web del Instituto Distrital de Recreación y Deporte IDRD y demás medios de comunicación radiales y de televisión lo que se considere necesarios; en el marco de la participación del programa. Con mi firma doy constancia de que leí, entendí y acepto el contenido del presente documento." );
                        });

                        $row = $f1;
                    }
                    $row++;
                    $x++;
                }

                $sum = $row-1;

                $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                );
                $sheet->getStyle("A13:AX$sum")->applyFromArray($styleArray);
            });
        })->export('xlsx');

    }

    public function pdf( Request $request )
    {
        require_once ('FPDF/fpdf17/fpdf.php');
        require_once('FPDF/FPDI-1.5.4/fpdi.php');

        $pdf = new FPDI();
        $pdf->AddPage();
        $pdf->setSourceFile( public_path('excel/REPORTE_ASISTENCIA_PARTICIPANTES.pdf') );
        $tplIdx = $pdf->importPage(1);
        $pdf->useTemplate($tplIdx,null,null,null,null,true);
        $pdf->SetMargins(0, 0, 0, true);
        $pdf->SetTextColor(0,0,0);
        $pdf->SetFont('Arial','B', 5);

        $start = Carbon::parse( $request->get('start_day') )->format('Y-m-d');
        $end   = Carbon::parse( $request->get('end_day') )->format('Y-m-d') ;

        $user_id = $request->has('instructor') ? $request->get('instructor') : $_SESSION['Id_funcionario'];
        $user_name = Persona::query()->where('Id_Persona', $user_id)->first();
        $user_name = isset( $user_name->full_name ) ? $user_name->full_name : $_SESSION['auth']->full_name;

        $sessions = Sessions::query()->where('user_id', $user_id)->where('schedule', $request->get('schedule'))->where('park', $request->get('park') )->whereBetween('day', [$start, $end])->orderBy('day')->with('participants')->get();

        $days = [];
        $park = Stage::find( $request->get('park') );
        $park_name = (isset( $park->vc_escenario )) ? $park->vc_escenario : null;



        //Programa o actividad
        $pdf->SetXY(63.6,68.4);
        $pdf->Cell(0,0,  $this->toUpper('BOGOTÁ CORRE MEJOR PARA TODOS'), 0, 0,'L');

        //Parque o escenario
        $pdf->SetXY(63.6,72.2);
        $pdf->Cell(0,0,  $this->toUpper($park_name), 0, 0,'L');

        //Instructor
        $pdf->SetXY(182.6,68.4);
        $pdf->Cell(0,0,  $this->toUpper( $user_name ), 0, 0,'L');

        //Localidad
        $pdf->SetXY(182.6,72.2);
        $pdf->Cell(0,0,  $this->toUpper( $park_name ), 0, 0,'L');

        //Horario
        $pdf->SetXY(261,72.2);
        $pdf->Cell(0,0,  $this->toUpper( $request->get('schedule') ), 0, 0,'L');

        //periodo
        $pdf->SetXY(261,84.2);
        $pdf->Cell(0,0,  $this->toUpper( "PERIODO DEL $start AL $end " ), 0, 0,'L');

        //Programa o actividad
        $y = 106.7;
        $lines = 1;

        foreach ($sessions->pluck('day')->toArray() as $day) {
            $days[] = (int) Carbon::parse( $day )->format('d');
        }

        $pdf->SetXY(249, 99.1);
        $pdf->Cell(0,0,  $this->toUpper( isset( $days[0] ) ? $days[0] : null ), 0, 0,'L');
        $pdf->SetXY(252, 99.1);
        $pdf->Cell(0,0,  $this->toUpper( isset( $days[1] ) ? $days[1] : null ), 0, 0,'L');
        $pdf->SetXY(255, 99.1);
        $pdf->Cell(0,0,  $this->toUpper( isset( $days[2] ) ? $days[2] : null ), 0, 0,'L');
        $pdf->SetXY(258, 99.1);
        $pdf->Cell(0,0,  $this->toUpper( isset( $days[3] ) ? $days[3] : null ), 0, 0,'L');
        $pdf->SetXY(261.5, 99.1);
        $pdf->Cell(0,0,  $this->toUpper( isset( $days[4] ) ? $days[4] : null ), 0, 0,'L');
        $pdf->SetXY(264.5, 99.1);
        $pdf->Cell(0,0,  $this->toUpper( isset( $days[5] ) ? $days[5] : null ), 0, 0,'L');
        $pdf->SetXY(268, 99.1);
        $pdf->Cell(0,0,  $this->toUpper( isset( $days[6] ) ? $days[6] : null ), 0, 0,'L');
        $pdf->SetXY(271, 99.1);
        $pdf->Cell(0,0,  $this->toUpper( isset( $days[7] ) ? $days[7] : null ), 0, 0,'L');
        $pdf->SetXY(274, 99.1);
        $pdf->Cell(0,0,  $this->toUpper( isset( $days[8] ) ? $days[8] : null ), 0, 0,'L');
        $pdf->SetXY(277, 99.1);
        $pdf->Cell(0,0,  $this->toUpper( isset( $days[9] ) ? $days[9] : null ), 0, 0,'L');
        $pdf->SetXY(280.5, 99.1);
        $pdf->Cell(0,0,  $this->toUpper( isset( $days[10] ) ? $days[10] : null ), 0, 0,'L');
        $pdf->SetXY(283.5, 99.1);
        $pdf->Cell(0,0,  $this->toUpper( isset( $days[11] ) ? $days[11] : null ), 0, 0,'L');
        $pdf->SetXY(286.5, 99.1);
        $pdf->Cell(0,0,  $this->toUpper( isset( $days[12] ) ? $days[12] : null ), 0, 0,'L');
        $pdf->SetXY(290, 99.1);
        $pdf->Cell(0,0,  $this->toUpper( isset( $days[13] ) ? $days[13] : null ), 0, 0,'L');
        $pdf->SetXY(293, 99.1);
        $pdf->Cell(0,0,  $this->toUpper( isset( $days[14] ) ? $days[14] : null ), 0, 0,'L');


        $participants = ParticipantView::has('attendance')->with([
            'attendance' => function($q) use( $sessions ) {
                return $q->whereHas('sessions', function ($query) use( $sessions ) {
                    return $query->whereIn('id', $sessions->pluck('id')->toArray() ) ;
                })->with('sessions');
            }
        ])->get();


        $arr = [];
        $i = 1;
        $row = 13;
        $c = 0;

        if ( $participants ) {
            foreach ( $participants as $participant ) {
                if ( count( $participant->attendance ) > 0 ) {
                    $pdf->SetXY(13.6, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->name, $participant->lastname ) ? "{$participant->name} {$participant->lastname}" : null), 0, 0,'L');
                    $pdf->SetXY(63.6, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->document_type_name ) ? $participant->document_type_name : null ), 0, 0,'L');
                    $pdf->SetXY(78.6, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->document ) ? $participant->document : null), 0, 0,'L');
                    $pdf->SetXY(98.6, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->birthday ) ? Carbon::now()->diffInYears( Carbon::parse( $participant->birthday ) ) : null), 0, 0,'L');
                    $pdf->SetXY(105.6, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->location_name ) ? $participant->location_name : null), 0, 0,'L');
                    $pdf->SetXY(125.6, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->phone ) ? $participant->phone : null), 0, 0,'L');
                    $pdf->SetXY(141.6, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->eps ) ? $participant->eps : null), 0, 0,'L');

                    $pdf->SetXY(171, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->sex_name ) &&  $participant->sex_name == 'MUJER' ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(174, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->sex_name ) &&  $participant->sex_name == 'HOMBRE' ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(177, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->sex_name ) &&  $participant->gender_name == 'FEMENINO' ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(180, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->sex_name ) &&  $participant->gender_name == 'MASCULINO' ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(183, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->sex_name ) &&  $participant->gender_name == 'TRANSGENERO' ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(187, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->ethnic_group_name ) && $participant->ethnic_group_name == 'AFRO' ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(190, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->ethnic_group_name ) && $participant->ethnic_group_name == 'RAIZAL' ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(193, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->ethnic_group_name ) && $participant->ethnic_group_name == 'INDÍGENA' ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(196, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->ethnic_group_name ) && $participant->ethnic_group_name == 'GITANO' ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(199, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->ethnic_group_name ) && $participant->ethnic_group_name == 'MESTIZO' ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(202, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->social_group_id ) && (int) $participant->social_group_id == 1 ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(205, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->situation_id ) && (int) $participant->situation_id == 1 ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(208, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->situation_id ) && (int) $participant->situation_id == 2 ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(211, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->situation_id ) && (int) $participant->situation_id == 3 ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(215, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->situation_id ) && (int) $participant->situation_id == 4 ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(218, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->social_group_id ) && (int) $participant->social_group_id == 2 ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(221, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->social_group_id ) && (int) $participant->social_group_id == 3 ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(224, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->social_group_id ) && (int) $participant->social_group_id == 4 ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(227, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->social_group_id ) && (int) $participant->social_group_id == 5 ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(230, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->social_group_id ) && (int) $participant->social_group_id == 6 ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(233.5, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->situation_id ) && (int) $participant->situation_id == 6 ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(237, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->situation_id ) && (int) $participant->situation_id == 7 ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(240, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->situation_id ) && (int) $participant->situation_id == 8 ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(243, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->situation_id ) && (int) $participant->situation_id == 9 ? 'X' : null), 0, 0,'L');
                    $pdf->SetXY(246, $y);
                    $pdf->Cell(0,0,  $this->toUpper(isset( $participant->situation_id ) && (int) $participant->situation_id == 10 ? 'X' : null), 0, 0,'L');


                    $count = 0;
                    $spaces = count( $days );
                    foreach ( $days as $day ) {
                        $arr[$c][$day] = 'F';
                    }

                    foreach ( $participant->attendance as $value ) {
                        $key = isset( $value->sessions->day ) ? (int) Carbon::parse( $value->sessions->day )->format('d') : null;
                        $value = (isset( $value->attendance ) && $value->attendance == true ) ? $value->attendance : null;
                        if ( $value) {
                            $arr[$c][$key] = 'A';
                            $count++;
                        } else {
                            $arr[$c][$key] = 'F';
                        }
                    }

                    $assistances = array_values( $arr[$c] );

                    $pdf->SetXY(249.5, $y);
                    $pdf->Cell(0,0,  $this->toUpper( isset( $assistances[0] ) ? $assistances[0] : null ), 0, 0,'L');
                    $pdf->SetXY(252.5, $y);
                    $pdf->Cell(0,0,  $this->toUpper( isset( $assistances[1] ) ? $assistances[1] : null ), 0, 0,'L');
                    $pdf->SetXY(255.5, $y);
                    $pdf->Cell(0,0,  $this->toUpper( isset( $assistances[2] ) ? $assistances[2] : null ), 0, 0,'L');
                    $pdf->SetXY(258.5, $y);
                    $pdf->Cell(0,0,  $this->toUpper( isset( $assistances[3] ) ? $assistances[3] : null ), 0, 0,'L');
                    $pdf->SetXY(261.5, $y);
                    $pdf->Cell(0,0,  $this->toUpper( isset( $assistances[4] ) ? $assistances[4] : null ), 0, 0,'L');
                    $pdf->SetXY(264.5, $y);
                    $pdf->Cell(0,0,  $this->toUpper( isset( $assistances[5] ) ? $assistances[5] : null ), 0, 0,'L');
                    $pdf->SetXY(268.5, $y);
                    $pdf->Cell(0,0,  $this->toUpper( isset( $assistances[6] ) ? $assistances[6] : null ), 0, 0,'L');
                    $pdf->SetXY(271.5, $y);
                    $pdf->Cell(0,0,  $this->toUpper( isset( $assistances[7] ) ? $assistances[7] : null ), 0, 0,'L');
                    $pdf->SetXY(274.5, $y);
                    $pdf->Cell(0,0,  $this->toUpper( isset( $assistances[8] ) ? $assistances[8] : null ), 0, 0,'L');
                    $pdf->SetXY(277.5, $y);
                    $pdf->Cell(0,0,  $this->toUpper( isset( $assistances[9] ) ? $assistances[9] : null ), 0, 0,'L');
                    $pdf->SetXY(280.5, $y);
                    $pdf->Cell(0,0,  $this->toUpper( isset( $assistances[10] ) ? $assistances[10] : null ), 0, 0,'L');
                    $pdf->SetXY(283.5, $y);
                    $pdf->Cell(0,0,  $this->toUpper( isset( $assistances[11] ) ? $assistances[11] : null ), 0, 0,'L');
                    $pdf->SetXY(286.5, $y);
                    $pdf->Cell(0,0,  $this->toUpper( isset( $assistances[12] ) ? $assistances[12] : null ), 0, 0,'L');
                    $pdf->SetXY(289.5, $y);
                    $pdf->Cell(0,0,  $this->toUpper( isset( $assistances[13] ) ? $assistances[13] : null ), 0, 0,'L');
                    $pdf->SetXY(293.5, $y);
                    $pdf->Cell(0,0,  $this->toUpper( isset( $assistances[14] ) ? $assistances[14] : null ), 0, 0,'L');


                    $pdf->SetXY(296.5, $y);
                    $pdf->Cell(0,0,  $this->toUpper($count), 0, 0,'L');


                    $y = $y + 4;
                    $lines++;
                    $row++;
                    $c++;

                    if ( $lines > 15 ) {
                        $lines = 1;
                        $y = 106.7;

                        $pdf->AddPage();
                        $pdf->setSourceFile( public_path('excel/REPORTE_ASISTENCIA_PARTICIPANTES.pdf') );
                        $tplIdx = $pdf->importPage(1);
                        $pdf->useTemplate($tplIdx,null,null,null,null,true);
                        $pdf->SetMargins(0, 0, 0, true);
                        $pdf->SetTextColor(0,0,0);
                        $pdf->SetFont('Arial','B', 5);

                        //Programa o actividad
                        $pdf->SetXY(63.6,68.4);
                        $pdf->Cell(0,0,  $this->toUpper('BOGOTÁ CORRE MEJOR PARA TODOS'), 0, 0,'L');

                        //Parque o escenario
                        $pdf->SetXY(63.6,72.2);
                        $pdf->Cell(0,0,  $this->toUpper($park_name), 0, 0,'L');

                        //Instructor
                        $pdf->SetXY(182.6,68.4);
                        $pdf->Cell(0,0,  $this->toUpper( $user_name ), 0, 0,'L');

                        //Localidad
                        $pdf->SetXY(182.6,72.2);
                        $pdf->Cell(0,0,  $this->toUpper( $park_name ), 0, 0,'L');

                        //Horario
                        $pdf->SetXY(261,72.2);
                        $pdf->Cell(0,0,  $this->toUpper( $request->get('schedule') ), 0, 0,'L');

                        //periodo
                        $pdf->SetXY(261,84.2);
                        $pdf->Cell(0,0,  $this->toUpper( "PERIODO DEL $start AL $end " ), 0, 0,'L');

                        $pdf->SetXY(249, 99.1);
                        $pdf->Cell(0,0,  $this->toUpper( isset( $days[0] ) ? $days[0] : null ), 0, 0,'L');
                        $pdf->SetXY(252, 99.1);
                        $pdf->Cell(0,0,  $this->toUpper( isset( $days[1] ) ? $days[1] : null ), 0, 0,'L');
                        $pdf->SetXY(255, 99.1);
                        $pdf->Cell(0,0,  $this->toUpper( isset( $days[2] ) ? $days[2] : null ), 0, 0,'L');
                        $pdf->SetXY(258, 99.1);
                        $pdf->Cell(0,0,  $this->toUpper( isset( $days[3] ) ? $days[3] : null ), 0, 0,'L');
                        $pdf->SetXY(261.5, 99.1);
                        $pdf->Cell(0,0,  $this->toUpper( isset( $days[4] ) ? $days[4] : null ), 0, 0,'L');
                        $pdf->SetXY(264.5, 99.1);
                        $pdf->Cell(0,0,  $this->toUpper( isset( $days[5] ) ? $days[5] : null ), 0, 0,'L');
                        $pdf->SetXY(268, 99.1);
                        $pdf->Cell(0,0,  $this->toUpper( isset( $days[6] ) ? $days[6] : null ), 0, 0,'L');
                        $pdf->SetXY(271, 99.1);
                        $pdf->Cell(0,0,  $this->toUpper( isset( $days[7] ) ? $days[7] : null ), 0, 0,'L');
                        $pdf->SetXY(274, 99.1);
                        $pdf->Cell(0,0,  $this->toUpper( isset( $days[8] ) ? $days[8] : null ), 0, 0,'L');
                        $pdf->SetXY(277, 99.1);
                        $pdf->Cell(0,0,  $this->toUpper( isset( $days[9] ) ? $days[9] : null ), 0, 0,'L');
                        $pdf->SetXY(280.5, 99.1);
                        $pdf->Cell(0,0,  $this->toUpper( isset( $days[10] ) ? $days[10] : null ), 0, 0,'L');
                        $pdf->SetXY(283.5, 99.1);
                        $pdf->Cell(0,0,  $this->toUpper( isset( $days[11] ) ? $days[11] : null ), 0, 0,'L');
                        $pdf->SetXY(286.5, 99.1);
                        $pdf->Cell(0,0,  $this->toUpper( isset( $days[12] ) ? $days[12] : null ), 0, 0,'L');
                        $pdf->SetXY(290, 99.1);
                        $pdf->Cell(0,0,  $this->toUpper( isset( $days[13] ) ? $days[13] : null ), 0, 0,'L');
                        $pdf->SetXY(293, 99.1);
                        $pdf->Cell(0,0,  $this->toUpper( isset( $days[14] ) ? $days[14] : null ), 0, 0,'L');
                    }
                }
            }
        }
        $pdf->Output("Asistencia_PERIODO_$start $end.pdf", 'I');
    }

    public function toUpper( $string = null )
    {
        return utf8_decode( mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_UPPER, 'UTF-8') );
    }
}
