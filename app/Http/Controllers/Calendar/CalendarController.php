<?php

namespace App\Http\Controllers\Calendar;

use App\ActivityAccess;
use App\Persona;
use App\Sessions;
use App\Transformers\SessionTransformer;
use Carbon\Carbon;
use Idrd\Usuarios\Repo\ActividadesSim;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class CalendarController extends Controller
{
    public function index(Request $request)
    {
        $start = $request->has( 'start' ) ? $request->get('start') : Carbon::now()->startOfMonth();
        $end = $request->has( 'end' ) ? $request->get('end') : Carbon::now()->endOfMonth();
        $user_id = $request->has('instructor') ? $request->get('instructor') : $_SESSION['Id_funcionario'];

        $programming = Sessions::query()->where('user_id', $user_id)->whereBetween( 'day', [$start, $end] )->get();
        $resource = new Collection($programming, new SessionTransformer() );
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json( $rootScope->toArray()['data'], 200 );
    }

    public function instructors()
    {
        $permissions = ActividadesSim::query()->where( 'Id_Modulo', env('MODULE_ID') )->get()->pluck('Id_Actividad')->toArray();
        $perms = ActivityAccess::query()->whereIn('Id_Actividad', $permissions)->get()->pluck('Id_Persona')->toArray();

        $instructors = Persona::query()->whereIn('Id_Persona', $perms)->get();

        $data = [];

        foreach ( $instructors as $instructor ) {
            $data[] = [
                'id'    =>  isset( $instructor->Id_Persona ) ? $instructor->Id_Persona : null,
                'name'  =>  isset( $instructor->part_name ) ? $instructor->part_name : null
            ];
        }

        return response()->json([
            'data'  =>  $data,
            'code'  =>  200
        ], 200);
    }
}
