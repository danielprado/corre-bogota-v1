<?php

namespace App\Http\Controllers\Counter;

use App\Participant;
use App\Sessions;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

class CounterController extends Controller
{
    public function index()
    {
        $is_admin = isset( $_SESSION['Usuario']['Permisos']['admin_access'] ) && intval( $_SESSION['Usuario']['Permisos']['admin_access'] ) == 1;

        $sessions = $is_admin
                  ? Sessions::count()
                  : Sessions::where('user_id', $_SESSION['Id_funcionario'])->count();

        $weekly_sessions  = $is_admin
                            ? Sessions::whereBetween(
                                    'day', [ Carbon::now()->startOfWeek()->format('Y-m-d'), Carbon::now()->endOfWeek()->format('Y-m-d')  ]
                                )->count()
                            : Sessions::where('user_id', $_SESSION['Id_funcionario'])->whereBetween(
                                    'day', [ Carbon::now()->startOfWeek()->format('Y-m-d'), Carbon::now()->endOfWeek()->format('Y-m-d')  ]
                                )->count();

        $monthly_sessions = $is_admin
                            ? Sessions::whereBetween(
                                    'day', [ Carbon::now()->startOfMonth()->format('Y-m-d'), Carbon::now()->endOfMonth()->format('Y-m-d')  ]
                                )->count()
                            : Sessions::where('user_id', $_SESSION['Id_funcionario'])->whereBetween(
                                    'day', [ Carbon::now()->startOfMonth()->format('Y-m-d'), Carbon::now()->endOfMonth()->format('Y-m-d')  ]
                                )->count();

        $arr = [
            'counter_people'    =>  Participant::count(),
            'counter_sessions'  =>  $sessions,
            'counter_week'      =>  $weekly_sessions,
            'counter_month'     =>  $monthly_sessions,
        ];

        return response()->json([
            'data'  =>  $arr,
            'code'  =>  200
        ], 200);
    }
}
