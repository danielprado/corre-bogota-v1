<?php

namespace App\Http\Controllers\Situation;

use App\Situation;
use App\Transformers\SituationTransformer;

use App\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class SituationController extends Controller
{
    public function index()
    {
        $resource = Situation::all();
        $resource = new Collection($resource, new SituationTransformer());
        $manager = new Manager();
        $resource = $manager->createData($resource);
        return response()->json($resource->toArray(), 200);
    }

    public function show( $id )
    {
        $resource = Situation::where('population_id', $id)->get();
        $resource = new Collection($resource, new SituationTransformer());
        $manager = new Manager();
        $resource = $manager->createData($resource);
        return response()->json($resource->toArray(), 200);
    }
}
