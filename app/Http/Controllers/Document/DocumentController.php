<?php

namespace App\Http\Controllers\Document;

use App\Document;
use App\Transformers\DocumentTransformer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $documents = Document::whereIn('Id_TipoDocumento',[1,4,6])->get();
        $resource = new Collection($documents, new DocumentTransformer());
        $manager = new Manager();
        $rootScope = $manager->createData($resource);
        return response()->json($rootScope->toArray(), 200);
    }
}
