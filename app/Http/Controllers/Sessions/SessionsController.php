<?php

namespace App\Http\Controllers\Sessions;

use App\Sessions;
use App\Stage;
use App\Transformers\SessionTransformer;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;

class SessionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $column = \request()->has( 'column' ) ? \request()->get('column') : 'id';
        $order = \request()->has( 'order' ) ? \request()->get('order') : false;
        $per_page = \request()->has( 'per_page' ) ? \request()->get('per_page') : 5;
        $query = \request()->has( 'query' ) ? \request()->get('query') : null;
        $order =  ( $order ) ? 'asc' : 'desc';

        $user_id = \request()->has( 'instructor' ) ? \request()->get('instructor') : null;

        $is_admin = isset( $_SESSION['Usuario']['Permisos']['admin_access'] ) && intval( $_SESSION['Usuario']['Permisos']['admin_access'] ) == 1;
        $is_not_admin = !$is_admin;

        $sessions = Sessions::query()->when( $is_not_admin, function ( $query ) {
            return $query->where('user_id', $_SESSION['Id_funcionario']);
        })->when( $user_id && $is_admin, function ($query) use ( $user_id ) {
            return $query->where('user_id', $user_id);
        });

        if ( $query ) {
            $parks = Stage::query()->where('vc_escenario', 'LIKE', "%{$query}%")
                                    ->get()->pluck('i_pk_id')->toArray();
            $sessions = $sessions->where('id', 'LIKE', "%{$query}%")
                                  ->orWhere('schedule', 'LIKE', "%{$query}%")
                                  ->orWhere('day', 'LIKE', "%{$query}%")
                                  ->orWhereIn('park', $parks);


        }

        $sessions = $sessions->orderBy( $column, $order )->paginate( $per_page );

        $resource = $sessions->getCollection()
            ->map(function($model) use( $is_not_admin, $is_admin, $user_id ) {
                if ( $is_not_admin && $model->user_id == $_SESSION['Id_funcionario'] ) {
                    return ( new SessionTransformer() )->transform( $model );
                } else if ( $is_admin && $user_id && $model->user_id == $user_id ) {
                    return ( new SessionTransformer() )->transform( $model );
                } else {
                    return ( new SessionTransformer() )->transform( $model );
                }
            })->toArray();

        $data = new LengthAwarePaginator(
            $resource,
            (int) $sessions->total(),
            (int) $sessions->perPage(),
            (int) $sessions->currentPage(), [
            'path' => request()->url(),
            'query' => [
                'page' => (int) $sessions->currentPage()
            ]
        ]);

        return response()->json([
            'data'  =>  $data,
            'admin' =>  $is_admin,
            'code'  =>  200
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\StoreSessionRequest $request
     * @return Response
     */
    public function store(Requests\StoreSessionRequest $request)
    {
        $exists = Sessions::query()->where('day', $request->get('day'))
                                    ->where('park', $request->get('park'))
                                    ->where('schedule', $request->get('schedule'))
                                    ->where('initial_hour', $request->get('initial_time'))
                                    ->where('final_hour', $request->get('end_time'))
                                    ->where('user_id', $_SESSION['Id_funcionario'])->first();

        if ( $exists ) {
            return response()->json([
                'message'  =>  trans('validation.handler.exists'),
                'code'  => 422
            ], 422);
        }



        $sessions = new Sessions();
        $sessions->park = $request->get('park');

        if ( $request->has('initial_time') ) {
            $sessions->schedule = Carbon::parse( $request->get('initial_time') )->format('A');
        }

        $sessions->day = $request->get('day');
        $sessions->initial_hour = $request->get('initial_time');
        $sessions->final_hour = $request->get('end_time');
        $sessions->user_id = $_SESSION['Id_funcionario'];
        if ( $sessions->save() ) {
            $sessions->attendance()->attach( $request->get('participants') );

            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  => 200
            ], 200);
        }

        return response()->json([
            'message'  =>  trans('validation.handler.validation_failed'),
            'code'  => 422
        ], 422);
    }

    /**
     * Display the specified resource.
     *
     * @param Sessions $sessions
     * @return Response
     */
    public function show(Sessions $sessions)
    {
        $is_admin = isset( $_SESSION['Usuario']['Permisos']['admin_access'] ) && intval( $_SESSION['Usuario']['Permisos']['admin_access'] ) == 1;

        if ( $sessions->user_id == $_SESSION['Id_funcionario'] || $is_admin  ) {
            $resource = new Item($sessions, new SessionTransformer());
            $manager = new Manager();
            $resource = $manager->createData($resource);
            return response()->json($resource->toArray(), 200);
        }
        return response()->json([
            'message'  =>  trans('validation.handler.resource_not_found'),
            'code'  => 404
        ], 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\StoreSessionRequest $request
     * @param Sessions $sessions
     * @return void
     */
    public function update(Requests\StoreSessionRequest $request, Sessions $sessions)
    {
        $is_admin = isset( $_SESSION['Usuario']['Permisos']['admin_access'] ) && intval( $_SESSION['Usuario']['Permisos']['admin_access'] ) == 1;

        if ( $sessions->user_id == $_SESSION['Id_funcionario'] || $is_admin) {

            $exists = Sessions::query()->where('day', $request->get('day'))
                ->where('park', $request->get('park'))
                ->where('schedule', $request->get('schedule'))
                ->where('initial_hour', $request->get('initial_time'))
                ->where('final_hour', $request->get('end_time'))
                ->where('id', '!=', $sessions->id)
                ->where('user_id', $_SESSION['Id_funcionario'])->first();

            if ( $exists ) {
                return response()->json([
                    'message'  =>  trans('validation.handler.exists'),
                    'code'  => 422
                ], 422);
            }

            if (  $sessions->attendance()->detach() ) {
                $sessions->park = $request->get('park');
                if ( $request->has('initial_time') ) {
                    $sessions->schedule = Carbon::parse( $request->get('initial_time') )->format('A');
                }
                $sessions->day = $request->get('day');
                $sessions->initial_hour = $request->get('initial_time');
                $sessions->final_hour = $request->get('end_time');
                if ( $sessions->save() ) {
                    $sessions->attendance()->attach( $request->get('participants') );

                    return response()->json([
                        'data'  =>  trans('validation.handler.success'),
                        'code'  => 200
                    ], 200);
                }
            }

            return response()->json([
                'message'  =>  trans('validation.handler.validation_failed'),
                'code'  => 422
            ], 422);
        } else {
            return response()->json([
                'message'  =>  trans('validation.handler.resource_not_found'),
                'code'  => 404
            ], 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Sessions $sessions
     * @return Response
     * @throws Exception
     */
    public function destroy(Sessions $sessions)
    {
        $is_admin = isset( $_SESSION['Usuario']['Permisos']['admin_access'] ) && intval( $_SESSION['Usuario']['Permisos']['admin_access'] ) == 1;
        
        if ( $sessions->user_id == $_SESSION['Id_funcionario'] || $is_admin ) {
            if (  $sessions->attendance()->detach() ) {
                if ( $sessions->delete() ) {
                    return response()->json([
                        'data'  =>  trans('validation.handler.deleted'),
                        'code'  => 200
                    ], 200);
                }
            }
            return response()->json([
                'message'  =>  trans('validation.handler.validation_failed'),
                'code'  => 422
            ], 422);
        }

        return response()->json([
            'message'  =>  trans('validation.handler.resource_not_found'),
            'code'  => 404
        ], 404);
    }
}
