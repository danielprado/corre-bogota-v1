<?php

namespace App\Http\Controllers\InitalData;

use App\Document;
use App\Ethnic;
use App\Gender;
use App\Location;
use App\Sex;
use App\Situation;
use App\SocialGroup;
use App\Transformers\DocumentTransformer;
use App\Transformers\EthnicTransformer;
use App\Transformers\GenderTransformer;
use App\Transformers\LocationTransformer;
use App\Transformers\SexTransformer;

use App\Http\Controllers\Controller;
use App\Transformers\SituationTransformer;
use App\Transformers\SocialGroupTransformer;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class InitialDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $manager = new Manager();

        $document_types = Document::whereIn('Id_TipoDocumento',[1,4,6])->get();
        $document_types = new Collection($document_types, new DocumentTransformer());
        $document_types = $manager->createData($document_types);

        $sex = Sex::all();
        $sex = new Collection($sex, new SexTransformer());
        $sex = $manager->createData($sex);

        $gender = Gender::all();
        $gender = new Collection($gender, new GenderTransformer());
        $gender = $manager->createData($gender);

        $ethnic_groups = Ethnic::all();
        $ethnic_groups = new Collection($ethnic_groups, new EthnicTransformer());
        $ethnic_groups = $manager->createData($ethnic_groups);

        $social_groups = SocialGroup::all();
        $social_groups = new Collection($social_groups, new SocialGroupTransformer());
        $social_groups = $manager->createData($social_groups);

        $residence_locations = Location::all();
        $residence_locations = new Collection($residence_locations, new LocationTransformer());
        $residence_locations = $manager->createData($residence_locations);

        $population = Situation::all();
        $population = new Collection($population, new SituationTransformer());
        $population = $manager->createData($population);

        return response()->json([
            'data'  => [
                'document_types'    =>  $document_types->toArray()['data'],
                'sex'               =>  $sex->toArray()['data'],
                'genders'           =>  $gender->toArray()['data'],
                'ethnic_groups'     =>  $ethnic_groups->toArray()['data'],
                'social_groups'     =>  $social_groups->toArray()['data'],
                'residence_locations' => $residence_locations->toArray()['data'],
                'population'        => $population->toArray()['data'],
            ],
            'code'  =>  200,
        ], 200);
    }
}
