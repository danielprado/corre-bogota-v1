<?php

namespace App\Http\Controllers\Evidence;

use App\Evidence;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Transformers\EvidenceTransformer;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class EvidenceController extends Controller
{

    private $evidence;

    public function __construct()
    {
        $this->evidence = new Evidence();
    }

    public function indexAgreement()
    {
        $column = \request()->has( 'column' ) ? \request()->get('column') : 'id';
        $order = \request()->has( 'order' ) ? \request()->get('order') : false;
        $per_page = \request()->has( 'per_page' ) ? \request()->get('per_page') : 5;
        $query = \request()->has( 'query' ) ? \request()->get('query') : null;
        $order =  ( $order ) ? 'asc' : 'desc';
        $user_id = request()->has('instructor') ? request()->get('instructor') : $_SESSION['Id_funcionario'];
        $month = request()->has('month') ? request()->get('month') : null;


        $files = Evidence::query()->where('filetype', Evidence::AGREEMENT)
                                  ->where('user_id', $user_id);

        if ( $query ) {
            $files = $files->where('filename', 'LIKE', "%{$query}%")
                           ->orWhere('month','LIKE', "%{$query}%");
        }

        if ( $month ) {
            $files = $files->where('month', $month);
        }

        $files = $files->orderBy( 'month', 'desc' )->paginate( $per_page );


        $resource = $files->getCollection()
            ->map(function($model) use ( $user_id ) {
                if ( $model->user_id == $user_id ) {
                    return ( new EvidenceTransformer() )->transform( $model );
                }
            })->toArray();

        $data = new LengthAwarePaginator(
            $resource,
            (int) $files->total(),
            (int) $files->perPage(),
            (int) $files->currentPage(), [
            'path' => request()->url(),
            'query' => [
                'page' => (int) $files->currentPage()
            ]
        ]);

        return response()->json([
            'data'  =>  $data,
            'code'  =>  200
        ], 200);
    }

    public function indexWork()
    {
        $column = \request()->has( 'column' ) ? \request()->get('column') : 'id';
        $order = \request()->has( 'order' ) ? \request()->get('order') : false;
        $per_page = \request()->has( 'per_page' ) ? \request()->get('per_page') : 5;
        $query = \request()->has( 'query' ) ? \request()->get('query') : null;
        $order =  ( $order ) ? 'asc' : 'desc';
        $user_id = request()->has('instructor') ? request()->get('instructor') : $_SESSION['Id_funcionario'];
        $month = request()->has('month') ? request()->get('month') : null;

        $files = Evidence::query()->where('filetype', Evidence::WORK_PLAN)
            ->where('user_id', $user_id);

        if ( $query ) {
            $files = $files->where('filename', 'LIKE', "%{$query}%")
                           ->orWhere('month','LIKE', "%{$query}%");
        }

        if ( $month ) {
            $files = $files->where('month', $month);
        }

        $files = $files->orderBy( $column, $order )->paginate( $per_page );

        $resource = $files->getCollection()
            ->map(function($model) use ($user_id) {
                if ( $model->user_id == $user_id ) {
                    return ( new EvidenceTransformer() )->transform( $model );
                }
            })->toArray();

        $data = new LengthAwarePaginator(
            $resource,
            (int) $files->total(),
            (int) $files->perPage(),
            (int) $files->currentPage(), [
            'path' => request()->url(),
            'query' => [
                'page' => (int) $files->currentPage()
            ]
        ]);

        return response()->json([
            'data'  =>  $data,
            'code'  =>  200
        ], 200);
    }

    public function indexTraining()
    {
        $column = \request()->has( 'column' ) ? \request()->get('column') : 'id';
        $order = \request()->has( 'order' ) ? \request()->get('order') : false;
        $per_page = \request()->has( 'per_page' ) ? \request()->get('per_page') : 5;
        $query = \request()->has( 'query' ) ? \request()->get('query') : null;
        $order =  ( $order ) ? 'asc' : 'desc';
        $user_id = request()->has('instructor') ? request()->get('instructor') : $_SESSION['Id_funcionario'];
        $month = request()->has('month') ? request()->get('month') : null;

        $files = Evidence::query()->where('filetype', Evidence::TRAINING_PLAN)
                                  ->where('user_id', $user_id);

        if ( $query ) {
            $files = $files->where('filename', 'LIKE', "%{$query}%")
                           ->orWhere('month','LIKE', "%{$query}%");
        }

        if ( $month ) {
            $files = $files->where('month', $month);
        }

        $files = $files->orderBy( $column, $order )->paginate( $per_page );

        $resource = $files->getCollection()
            ->map(function($model) use ( $user_id ) {
                if ( $model->user_id == $user_id ) {
                    return ( new EvidenceTransformer() )->transform( $model );
                }
            })->toArray();

        $data = new LengthAwarePaginator(
            $resource,
            (int) $files->total(),
            (int) $files->perPage(),
            (int) $files->currentPage(), [
            'path' => request()->url(),
            'query' => [
                'page' => (int) $files->currentPage()
            ]
        ]);

        return response()->json([
            'data'  =>  $data,
            'code'  =>  200
        ], 200);
    }

    public function image(Requests\StoreImageRequest $request)
    {
        $name = substr_replace(sha1(microtime(true)), '', 20).'.'.$request->file('image')->getClientOriginalExtension();

        if ( $request->file('image')->move( public_path('storage/photo'), $name ) ) {
            $this->evidence->create([
                'filename'    =>  $request->file('image')->getClientOriginalName(),
                'filetype'    =>  Evidence::PHOTO,
                'path'        =>  $name,
                'description' =>  $request->get('description'),
                'user_id'     =>  $_SESSION['Id_funcionario'],
                'session_id'  =>  $request->get('session_id'),
            ]);
            return response()->json([
                'data' =>  trans('validation.handler.success'),
                'code'  =>  200
            ], 200);
        }
        return response()->json([
            'message' =>  trans('validation.handler.unexpected_failure'),
            'code'  =>  422
        ], 422);
    }

    public function agreements(Requests\StoreFileRequest $request){

        $name = substr_replace(sha1(microtime(true)), '', 20).'.'.$request->file('file')->getClientOriginalExtension();

        if ( $request->file('file')->move( public_path('storage/agreement'), $name ) ) {
            $this->evidence->create([
                'filename'    =>  $request->file('file')->getClientOriginalName(),
                'filetype'    =>  Evidence::AGREEMENT,
                'path'        =>  $name,
                'month'       =>  $request->get('month'),
                'user_id'     =>  $_SESSION['Id_funcionario']
            ]);
            return response()->json([
                'data' =>  trans('validation.handler.success'),
                'code'  =>  200
            ], 200);
        }
        return response()->json([
            'message' =>  trans('validation.handler.unexpected_failure'),
            'code'  =>  422
        ], 422);
    }

    public function work(Requests\StoreFileRequest $request){
        $name = substr_replace(sha1(microtime(true)), '', 20).'.'.$request->file('file')->getClientOriginalExtension();

        if ( $request->file('file')->move( public_path('storage/work_plan'), $name ) ) {
            $this->evidence->create([
                'filename'    =>  $request->file('file')->getClientOriginalName(),
                'filetype'    =>  Evidence::WORK_PLAN,
                'month'       =>  $request->get('month'),
                'path'        =>  $name,
                'user_id'     =>  $_SESSION['Id_funcionario'],
            ]);
            return response()->json([
                'data' =>  trans('validation.handler.success'),
                'code'  =>  200
            ], 200);
        }
        return response()->json([
            'message' =>  trans('validation.handler.unexpected_failure'),
            'code'  =>  422
        ], 422);
    }

    public function training(Requests\StoreFileRequest $request){
        $name = substr_replace(sha1(microtime(true)), '', 20).'.'.$request->file('file')->getClientOriginalExtension();

        if ( $request->file('file')->move( public_path('storage/training_plan'), $name ) ) {
            $this->evidence->create([
                'filename'    =>  $request->file('file')->getClientOriginalName(),
                'filetype'    =>  Evidence::TRAINING_PLAN,
                'month'       =>  $request->get('month'),
                'path'        =>  $name,
                'user_id'     =>  $_SESSION['Id_funcionario'],
            ]);
            return response()->json([
                'data' =>  trans('validation.handler.success'),
                'code'  =>  200
            ], 200);
        }
        return response()->json([
            'message' =>  trans('validation.handler.unexpected_failure'),
            'code'  =>  422
        ], 422);
    }

    public function update(Requests\UpdateEvidenceRequest $request, Evidence $evidence)
    {
        $evidence->month = $request->get('month');

        if ( $evidence->save() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  => 200
            ], 200);
        }

        return response()->json([
            'message'  =>  trans('validation.handler.validation_failed'),
            'code'  => 422
        ], 422);
    }

    public function destroy( Evidence $evidence )
    {
        if ( file_exists( public_path("storage/{$evidence->filetype}/$evidence->path") ) ) {
            unlink( public_path("storage/{$evidence->filetype}/$evidence->path") );
        }

        if ( $evidence->delete() ) {
            return response()->json([
                'data' =>  trans('validation.handler.deleted'),
                'code'  =>  204
            ], 200);
        }
        return response()->json([
            'message' =>  trans('validation.handler.unexpected_failure'),
            'code'  =>  422
        ], 422);
    }
}
