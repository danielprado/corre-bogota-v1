<?php

namespace App\Http\Controllers\Participant;

use App\Attendance;
use App\Participant;
use App\ParticipantView;
use App\Sessions;
use App\Transformers\ParticipantViewTransform;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Style_Border;

class ParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $column = request()->has( 'column' ) ? request()->get('column') : 'id';
        $order = request()->has( 'order' ) ? request()->get('order') : false;
        $per_page = request()->has( 'per_page' ) ? request()->get('per_page') : 5;
        $query = request()->has( 'query' ) ? request()->get('query') : null;

        $order =  ( $order ) ? 'asc' : 'desc';

        $participants = ParticipantView::query();

        if ( $query ) {
            $participants = $participants->where('id', 'LIKE', "%{$query}%")
                                        ->orWhere('document_type_name', 'LIKE', "%{$query}%")
                                        ->orWhere('document', 'LIKE', "%{$query}%")
                                        ->orWhere('name', 'LIKE', "%{$query}%")
                                        ->orWhere('lastname', 'LIKE', "%{$query}%")
                                        ->orWhere('birthday', 'LIKE', "%{$query}%")
                                        ->orWhere('email', 'LIKE', "%{$query}%")
                                        ->orWhere('sex_name', 'LIKE', "%{$query}%")
                                        ->orWhere('gender_name', 'LIKE', "%{$query}%")
                                        ->orWhere('ethnic_group_name', 'LIKE', "%{$query}%")
                                        ->orWhere('social_group_name', 'LIKE', "%{$query}%")
                                        ->orWhere('situation_name', 'LIKE', "%{$query}%")
                                        ->orWhere('eps', 'LIKE', "%{$query}%")
                                        ->orWhere('location_name', 'LIKE', "%{$query}%")
                                        ->orWhere('phone', 'LIKE', "%{$query}%")
                                        ->orWhere('created_at', 'LIKE', "%{$query}%");
        }

        $participants = $participants->orderBy( $column, $order )->paginate( $per_page );

        $resource = $participants->getCollection()
            ->map(function($model) {
                return ( new ParticipantViewTransform() )->transform( $model );
            })->toArray();

        $data = new LengthAwarePaginator(
            $resource,
            (int) $participants->total(),
            (int) $participants->perPage(),
            (int) $participants->currentPage(), [
            'path' => request()->url(),
            'query' => [
                'page' => (int) $participants->currentPage()
            ]
        ]);

        return response()->json([
            'data'  =>  $data,
            'code'  =>  200
        ], 200);
    }

    /**
     * Display a listing of the resource filtered by stage.
     *
     * @param $id
     * @param $schedule
     * @return Response
     */
    public function byStage( $id, $schedule )
    {
        $sessions = Sessions::query()->where('park', $id)->where('schedule', $schedule)->where('user_id', $_SESSION['Id_funcionario'])->get(['id'])->unique('id')->pluck('id')->toArray();
        $users = Attendance::query()->whereIn('session_id', $sessions)->get(['participant_id'])->unique('participant_id')->pluck('participant_id');
        $participants = ParticipantView::query()->whereIn('id', $users)->orderBy('name')->get(['id', 'name', 'lastname']);
        return response()->json([
            'data'  =>  $participants,
            'code'  =>  200
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\StoreParticipantRequest $request
     * @return void
     */
    public function store(Requests\StoreParticipantRequest $request)
    {
        $participant = Participant::query()->where('document', $request->get('document'))->first();

        if ( $participant ) {
            $participant->document_type_id = $request->get('document_type');
            $participant->document = $request->get('document');
            $participant->name = $request->get('name');
            $participant->lastname = $request->get('lastname');
            $participant->birthday = $request->get('birthday');
            $participant->email = $request->get('email');
            $participant->sex_id = $request->get('sex');
            $participant->gender_id = $request->get('gender');
            $participant->ethnic_group_id = $request->get('ethnic_group');
            $participant->social_group_id = $request->get('social_group');
            $participant->situation_id = $request->get('situation');
            $participant->eps = $request->get('eps');
            $participant->location_id = $request->get('residence_location');
            $participant->phone = $request->get('phone_or_mobile');
            if ( $participant->save() ) {
                return response()->json([
                    'message'  =>  trans('validation.handler.exists_participant'),
                    'code'  => 422
                ], 422);
            }
        }

        $participant = new Participant();
        $participant->document_type_id = $request->get('document_type');
        $participant->document = $request->get('document');
        $participant->name = $request->get('name');
        $participant->lastname = $request->get('lastname');
        $participant->birthday = $request->get('birthday');
        $participant->email = $request->get('email');
        $participant->sex_id = $request->get('sex');
        $participant->gender_id = $request->get('gender');
        $participant->ethnic_group_id = $request->get('ethnic_group');
        $participant->social_group_id = $request->get('social_group');
        $participant->situation_id = $request->get('situation');
        $participant->eps = $request->get('eps');
        $participant->location_id = $request->get('residence_location');
        $participant->phone = $request->get('phone_or_mobile');

        if ( $participant->save() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  => 200
            ], 200);
        }

        return response()->json([
            'message'  =>  trans('validation.handler.validation_failed'),
            'code'  => 422
        ], 422);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Participant $participants
     * @return void
     */
    public function update(Requests\StoreParticipantRequest $request, Participant $participants)
    {
        $participants->document_type_id = $request->get('document_type');
        $participants->document = $request->get('document');
        $participants->name = $request->get('name');
        $participants->lastname = $request->get('lastname');
        $participants->birthday = $request->get('birthday');
        $participants->email = $request->get('email');
        $participants->sex_id = $request->get('sex');
        $participants->gender_id = $request->get('gender');
        $participants->ethnic_group_id = $request->get('ethnic_group');
        $participants->social_group_id = $request->get('social_group');
        $participants->situation_id = $request->get('situation');
        $participants->eps = $request->get('eps');
        $participants->location_id = $request->get('residence_location');
        $participants->phone = $request->get('phone_or_mobile');

        if ( $participants->save() ) {
            return response()->json([
                'data'  =>  trans('validation.handler.success'),
                'code'  => 200
            ], 200);
        }

        return response()->json([
            'message'  =>  trans('validation.handler.validation_failed'),
            'code'  => 422
        ], 422);
    }

    public function show( ParticipantView $participants )
    {
        $resource = new Item($participants, new ParticipantViewTransform());
        $manager = new Manager();
        $resource = $manager->createData($resource);
        return response()->json($resource->toArray(), 200);
    }

    public function excel()
    {
        Excel::load( public_path('excel/REPORTE_PARTICIPANTES.xlsx'), function ($file) {
            $file->sheet(0, function ($sheet) {
                $row = 6;

                $sheet->cell("C3", function($cell)  {
                    $cell->setValue( Carbon::now()->format('Y-m-d H:i:s') );
                });

                $exports = ParticipantView::all()->toArray();
                $data = [];

                foreach ( $exports as $export ) {
                    $data[] = [
                        $export['id'],
                        $export['name'],
                        $export['lastname'],
                        $export['birthday'],
                        $export['document'],
                        $export['document_type_name'],
                        $export['sex_name'],
                        $export['gender_name'],
                        $export['ethnic_group_name'],
                        $export['social_group_name'],
                        $export['situation_name'],
                        $export['eps'],
                        $export['location_name'],
                        $export['phone'],
                        $export['email'],
                        ( new ParticipantViewTransform())->getStage( $export['id'] ),
                        ( new ParticipantViewTransform())->getTrainer( $export['id'] ),
                        $export['created_at'],
                    ];
                }

                foreach ( $data as $datum ) {
                    $sheet->row($row, $datum);
                    $row++;
                }

                $sum = $row-1;

                $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                );

                $sheet->getStyle("A6:R$sum")->applyFromArray($styleArray);
            });
        })->export('xlsx');
    }
}
