<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($_SESSION['auth']) {
            if ($request->ajax() || $request->wantsJson()) {
                return response()->json([
                    'message'   =>  trans('validation.handler.unauthenticated'),
                    'code'      =>  401
                ], 401);
            } else {
                return redirect()->route('logout');
            }
        }

        return $next($request);
    }
}
