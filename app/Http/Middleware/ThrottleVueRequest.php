<?php

namespace App\Http\Middleware;

use App\Traits\ApiResponse;
use Closure;
use Illuminate\Routing\Middleware\ThrottleRequests;
use Symfony\Component\HttpFoundation\Response;

class ThrottleVueRequest extends ThrottleRequests
{
    use ApiResponse;

    protected function buildResponse($key, $maxAttempts)
    {

        $retryAfter = $this->limiter->availableIn($key);

        $response = $this->errorResponse( trans_choice('validation.handler.max_attempts', $retryAfter, ['sec' => $retryAfter]), 429 );

        return $this->addHeaders(
            $response, $maxAttempts,
            $this->calculateRemainingAttempts($key, $maxAttempts, $retryAfter),
            $retryAfter
        );
    }
}
