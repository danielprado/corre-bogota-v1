<?php
session_start();
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//rutas con filtro de autenticación
//Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'MainController@welcome')->name('welcome');
    Route::any('/welcome', 'MainController@index');
    Route::post('login', 'MainController@login');
    Route::any('/logout', 'MainController@logout')->name('logout');
	Route::group(['middleware' => ['check.auth'] ], function () {
        Route::post('api/user/check', 'MainController@check');
        /* Routes for Vue */
        Route::group(['prefix' =>   'api'], function () {
            Route::get('user', 'MainController@auth');
            Route::post('reports/participants', 'Participant\ParticipantController@excel');
            Route::resource('participants', 'Participant\ParticipantController', [
                'only'  =>  ['index', 'show', 'store', 'update']
            ]);

            Route::get('participants/stage/{id}/{schedule}', 'Participant\ParticipantController@byStage');

            Route::resource('sessions', 'Sessions\SessionsController', [
                'only'  =>  ['index', 'show', 'store', 'update', 'destroy']
            ]);
            Route::resource('counters', 'Counter\CounterController', [
                'only'  =>  ['index']
            ]);
            Route::get('instructors', 'Calendar\CalendarController@instructors');
            Route::resource('calendar', 'Calendar\CalendarController', [
                'only'  =>  ['index']
            ]);

            Route::post('reports/attendance', 'Report\ReportController@pdf');

            Route::post('evidences/images', 'Evidence\EvidenceController@image');
            Route::post('evidences/agreements', 'Evidence\EvidenceController@agreements');
            Route::get('evidences/agreements', 'Evidence\EvidenceController@indexAgreement');

            Route::post('evidences/work-plan', 'Evidence\EvidenceController@work');
            Route::get('evidences/work-plan', 'Evidence\EvidenceController@indexWork');

            Route::post('evidences/training-plan', 'Evidence\EvidenceController@training');
            Route::get('evidences/training-plan', 'Evidence\EvidenceController@indexTraining');

            Route::resource('evidences', 'Evidence\EvidenceController', [
                'only'  =>  ['update', 'destroy'],
                'parameters' => [ 'evidences' => 'evidence' ]
            ]);

            Route::resource('attendance', 'Attendance\AttendanceController', [
                'only'  =>  ['update']
            ]);
            Route::resource('initial-data', 'InitalData\InitialDataController', [
                'only'  =>  ['index']
            ]);
            Route::resource('document-types', 'Document\DocumentController', [
                'only'  =>  ['index']
            ]);
            Route::resource('social-group', 'SocialGroup\SocialGroupController', [
                'only'  =>  ['index']
            ]);
            Route::resource('locations', 'Location\LocationController', [
                'only'  =>  ['index']
            ]);
            Route::resource('stage', 'Stage\StageController', [
                'only'  =>  ['index']
            ]);
            Route::resource('population', 'Situation\SituationController', [
                'only'  =>  ['index', 'show']
            ]);
            Route::resource('sex', 'Sex\SexController', [
                'only'  =>  ['index']
            ]);
            Route::resource('genders', 'Gender\GenderController', [
                'only'  =>  ['index']
            ]);
            Route::resource('ethnic-group', 'Ethnic\EthnicController', [
                'only'  =>  ['index']
            ]);
        });
    });
//});
