@extends('errors.illustrated-layout')

@section('title', trans('validation.handler.unexpected_failure'))
@section('code', '500')
@section('message', trans('validation.handler.unexpected_failure'))
@section('image')
    <div style="background-image: url({{ asset('public/svg/500.svg') }});" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center"></div>
@endsection
