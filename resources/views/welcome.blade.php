<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <!-- begin::Head -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">

        <!-- begin::Stylesheets -->

        <link rel="icon" href="{{ asset('public/favicon.ico') }}">
        <link rel="manifest" href="{{ asset('public/manifest.json') }}">

        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,400italic">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/all.css">
        <!-- end::Stylesheets -->

        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('public/apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('public/favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('public/site.webmanifest') }}">
        <link rel="mask-icon" href="{{ asset('public/safari-pinned-tab.svg') }}" color="#5bbad5">
        <meta name="apple-mobile-web-app-title" content="Corre Bogotá">
        <meta name="application-name" content="Corre Bogotá">
        <meta name="msapplication-TileColor" content="#0f1b2e">
        <meta name="msapplication-TileImage" content="{{ asset('public/mstile-144x144.png') }}">
        <meta name="theme-color" content="#ffffff">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @if( isset( $_SESSION['Usuario']['Permisos'] ) )
            <meta name="user-permissions" content="{{ json_encode( $_SESSION['Usuario']['Permisos'] ) }}">
        @endif

        <script type="text/javascript">
            @if( isset( $_SESSION['Usuario']['Permisos'] ) )
                window.isAuthenticated = true;
            @else
                window.isAuthenticated = false;
            @endif
        </script>

        <title>{{ config('app.name', 'Laravel') }}</title>
    </head>
    <!-- end::Head -->
    <!-- begin::Body -->
    <body class="sidebar-image" data-app>
        <!-- begin::Page -->
            <noscript>
                <strong>
                    {{ trans('auth.javascript') }}
                </strong>
            </noscript>
            <div id="app"></div>
        <!-- end::Page -->
        <!--begin::Global Theme Bundle -->
        <script type="application/javascript" src="{{ asset('public/js/app.js') }}?v={{ sha1_file( public_path('js/app.js') ) }}"></script>
        <!--end::Global Theme Bundle -->
    </body>
</html>