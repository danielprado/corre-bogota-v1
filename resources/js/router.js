import Vue from "vue";
import VueRouter from "vue-router";
import Auth from "@/package/auth"
Vue.use(VueRouter);
Vue.use(Auth);

import DashboardLayout from "./pages/Dashboard/Layout/DashboardLayout.vue";
import AuthLayout from "./pages/Dashboard/Auth/AuthLayout.vue";

const Login = () => import("./pages/Dashboard/Auth/Login.vue");
const Lock = () => import("./pages/Dashboard/Auth/Lock.vue");

const ErrorLayout = () =>
  import(/* webpackChunkName: "error" */ "@/pages/Dashboard/Error/ErrorLayout");

const Error = () =>
  import(/* webpackChunkName: "error" */ "@/pages/Dashboard/Error/Error");

import Report from "@/pages/Dashboard/Report/Report";

// Dashboard pages
import Dashboard from "./pages/Dashboard/Dashboard.vue";
import running from "./routes/running"
import session from "./routes/session"
import files from "./routes/files"

let authPages = {
  path: "/",
  redirect: "/lock",
  component: AuthLayout,
  name: "Authentication",
  children: [
    {
      path: "/lock",
      name: "Lock",
      component: Lock,
      meta: {
        forVisitors: true,
        can: true,
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: "/login",
      name: "Login",
      component: Login,
      meta: {
        forVisitors: true,
        can: true,
        //rtlActive: Vue.auth.isRTL
      }
    }
  ]
};

const routes = [
  authPages,
  {
    path: "/",
    redirect: "/dashboard",
    name: "Home",
    meta: {
      requiresAuth: true,
      can: window.isAuthenticated,
      //rtlActive: Vue.auth.isRTL
    }
  },
  {
    path: "/",
    component: DashboardLayout,
    children: [
      {
        path: "dashboard",
        name: "Dashboard",
        components: { default: Dashboard },
        meta: {
          requiresAuth: true,
          can: window.isAuthenticated,
          //rtlActive: Vue.auth.isRTL
        }
      },
      {
        path: "reports/attendances",
        name: "Attendance Report",
        component: Report ,
        meta: {
          requiresAuth: true,
          can: window.isAuthenticated,
          //rtlActive: Vue.auth.isRTL
        }
      }
    ]
  },
  running,
  session,
  files,
  /** Important, leave this route always at the end of the list to handle 404, 500 or another http errors **/
  {
    path: "/error",
    component: ErrorLayout,
    children: [
      {
        path: "",
        name: "Error",
        components: { default: Error },
        props: true
      }
    ]
  },
  {
    path: "**",
    redirect: "/error",
  },
];

export default new VueRouter({
  routes, // short for routes: routes
  linkExactActiveClass: "nav-item active"
});


