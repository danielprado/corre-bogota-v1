import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Init extends Model {
  constructor() {
    super(Api.END_POINTS.INIT_DATA(), {});
  }
}