import {Model} from "@/models/Model";
import {Api} from "@/models/Api";
import moment from "moment"

export class Participant extends Model {
  constructor(data){
    super( Api.END_POINTS.PARTICIPANTS(), data);

    this.validations = {
      required: {
        required: true,
      },
      email: {
        email: true,
      },
      date_format: {
        required: true,
        date_format: 'yyyy-MM-dd',
        date_between: {
          min: moment().subtract(80, 'years').format('YYYY-MM-DD'),
          max: moment().subtract(18, 'years').format('YYYY-MM-DD')
        }
      },
      number: {
        required: true,
        numeric: true
      },
      text: {
        required: true,
        alpha_spaces: true,
        min: 3
      },
      text_not_required: {
        alpha_spaces: true,
        min: 3
      },
      mobile: {
        min: 7,
        max: 10,
        numeric: true,
        required:true
      },
    };

    this.touched = {
      document_type: false,
      document: false,
      name: false,
      lastname: false,
      birthday: false,
      age: false,
      email: false,
      sex: false,
      gender: false,
      ethnic_group: false,
      social_group: false,
      situation: false,
      eps: false,
      residence_location: false,
      phone_or_mobile: false
    }

  }
  
  byStage( id, schedule ) {
    return this.get(`${Api.END_POINTS.PARTICIPANTS()}/stage/${id}/${schedule}`)
  }
  
}