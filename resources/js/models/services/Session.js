import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Session extends Model {
  constructor( data ) {
    super( Api.END_POINTS.SESSIONS(), data );

    this.validations = {
      required: {
        required: true,
      },
      numeric: {
        required: true,
        numeric: true,
      },
      hour_format: {
        required: true,
        date_format: 'HH:mm',
        before: 'end_time'
      },
      hour_format_end: {
        required: true,
        date_format: 'HH:mm',
      },
    }
    this.touched = {
      park: false,
      schedule: false,
      day: false,
      initial_time: false,
      end_time: false,
      participants: false
    }
  }
}