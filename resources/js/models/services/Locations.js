import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Locations extends Model {
  constructor() {
    super(Api.END_POINTS.LOCATIONS(), {});
  }
}