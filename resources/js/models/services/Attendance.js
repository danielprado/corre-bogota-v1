import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Attendance extends Model {
  constructor( data ) {
    super(Api.END_POINTS.ATTENDANCE(), data);
  }
}