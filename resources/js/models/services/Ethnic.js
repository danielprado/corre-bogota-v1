import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Ethnic extends Model {
  constructor() {
    super(Api.END_POINTS.ETHNIC_GROUP(), {});
  }
}