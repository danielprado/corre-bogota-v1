import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Files extends Model {
  constructor( data = {} ) {
    super(Api.END_POINTS.EVIDENCES(), data);
  }

  agreement( options = {} ) {
    return this.get( Api.END_POINTS.AGREEMENT(), options )
  }
  work( options = {} ) {
    return this.get( Api.END_POINTS.WORK(), options )
  }
  training( options = {} ) {
    return this.get( Api.END_POINTS.TRAINING(), options )
  }

}