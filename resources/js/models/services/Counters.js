import {Model} from "@/models/Model";
import {Api} from "@/models/Api";

export class Counters extends  Model {
  constructor() {
    super( Api.END_POINTS.COUNTERS(), {} )
  }
}
