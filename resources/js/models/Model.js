import { Form } from "./Form";

export class Model extends Form {
  constructor(url, data = {}) {
    super(data);
    this.url = url;
  }

  index(options = {}) {
    return this.get(this.url, options);
  }

  show(id, options = {}) {
    return this.get(`${this.url}/${id}`, options);
  }

  store(options = {}) {
    return this.post(this.url, options);
  }

  update(id, options = {}) {
    return this.put(`${this.url}/${id}`, options);
  }

  destroy(id, options = {}) {
    return this.delete(`${this.url}/${id}`, options);
  }
}
