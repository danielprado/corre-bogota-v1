export const Api = {
    COOKIE: process.env.MIX_COOKIE_NAME,
    END_POINTS: {
        //Data
        INSTRUCTORS: () => { return `${process.env.MIX_IDRD_URL}/api/instructors` },
        PARTICIPANTS: () => { return `${process.env.MIX_IDRD_URL}/api/participants` },
        SESSIONS: () => { return `${process.env.MIX_IDRD_URL}/api/sessions` },
        ATTENDANCE: () => { return `${process.env.MIX_IDRD_URL}/api/attendance` },
        IMAGES: () => { return `${process.env.MIX_IDRD_URL}/api/evidences/images` },
        AGREEMENT: () => { return `${process.env.MIX_IDRD_URL}/api/evidences/agreements` },
        EVIDENCES: (id) => { return `${process.env.MIX_IDRD_URL}/api/evidences` },
        WORK: () => { return `${process.env.MIX_IDRD_URL}/api/evidences/work-plan` },
        TRAINING: () => { return `${process.env.MIX_IDRD_URL}/api/evidences/training-plan` },
        COUNTERS: () => { return `${process.env.MIX_IDRD_URL}/api/counters` },
        CALENDAR: () => { return `${process.env.MIX_IDRD_URL}/api/calendar` },
        REPORT_PARTICIPANTS: () => { return `${process.env.MIX_IDRD_URL}/api/reports/participants` },
        REPORT_ATTENDANCE: () => { return `${process.env.MIX_IDRD_URL}/api/reports/attendance` },
        //Selects
        INIT_DATA: () => { return `${process.env.MIX_IDRD_URL}/api/initial-data` },
        DOCUMENT_TYPES: () => { return `${process.env.MIX_IDRD_URL}/api/document-types` },
        SOCIAL_GROUP: () => { return `${process.env.MIX_IDRD_URL}/api/social-group` },
        LOCATIONS: () => { return `${process.env.MIX_IDRD_URL}/api/locations` },
        SEX: () => { return `${process.env.MIX_IDRD_URL}/api/sex` },
        GENDERS: () => { return `${process.env.MIX_IDRD_URL}/api/genders` },
        ETHNIC_GROUP: () => { return `${process.env.MIX_IDRD_URL}/api/ethnic-group` },
        POPULATION: () => { return `${process.env.MIX_IDRD_URL}/api/population` },
        STAGE: () => { return `${process.env.MIX_IDRD_URL}/api/stage` },
        // Auth
        GET_PROFILE: () => { return `${process.env.MIX_IDRD_URL}/api/user` },
        CHECK_AUTH: () => { return `${process.env.MIX_IDRD_URL}/api/user/check` },
        LOGIN: () => { return `${process.env.MIX_IDRD_URL}/login` },
        LOGOUT: () => { return `${process.env.MIX_IDRD_URL}/logout` },
    }
};
