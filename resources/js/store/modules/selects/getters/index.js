const getters = {
    selectsHasBeenStored: state => ( state.document_types && state.sex &&state.genders && state.ethnic_groups && state.social_groups && state.residence_locations && state.population ),
    selectsHasNotBeenStored: state => ( !state.document_types && !state.sex && !state.genders && !state.ethnic_groups && !state.social_groups && !state.residence_locations && !state.population ),
    getStoredDocumentTypes: state => ( state.document_types ) ? state.document_types : null,
    getStoredSex: state => ( state.sex ) ? state.sex : null,
    getStoredGenders: state => ( state.genders ) ? state.genders : null,
    getStoredEthnicGroups: state => ( state.ethnic_groups ) ? state.ethnic_groups : null,
    getStoredSocialGroups: state => ( state.social_groups ) ? state.social_groups : null,
    getStoredResidenceLocations: state => ( state.residence_locations ) ? state.residence_locations : null,
    getStoredPopulation: state => ( state.population ) ? state.population : null,
};

export default getters;