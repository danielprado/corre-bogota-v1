const mutations = {
    INITIAL_DATA( state, response ) {
        state.document_types = response.data.document_types.map( (data) => {
            return {
                id: data.id,
                text: `${data.name} - ${data.description}`,
            }
        });
        state.sex  = response.data.sex;
        state.genders = response.data.genders;
        state.ethnic_groups = response.data.ethnic_groups;
        state.social_groups  = response.data.social_groups;
        state.residence_locations  = response.data.residence_locations;
        state.population  = response.data.population;
    },

    SET_DOCUMENT_TYPES(state, response) {
        state.document_types = response.data.map( (data) => {
            return {
                id: data.id,
                text: `${data.name} - ${data.description}`,
            }
        });
    },
    SET_SEX(state, response) {
        state.sex  = response.data
    },
    SET_GENDERS(state, response) {
        state.genders = response.data;
    },
    SET_ETHNIC_GROUPS(state, response) {
        state.ethnic_groups = response.data
    },
    SET_SOCIAL_GROUPS(state, response) {
        state.social_groups  = response.data
    },
    SET_RESIDENCE_LOCATIONS(state, response) {
        state.residence_locations  = response.data
    },
    SET_POPULATION(state, response) {
        state.population  = response.data
    },

    RESET_DOCUMENT_TYPES(state) {
        state.document_types = null;
    },
    RESET_SEX(state) {
        state.sex = null;
    },
    RESET_GENDERS(state) {
        state.genders = null;
    },
    RESET_ETHNIC_GROUPS(state) {
        state.ethnic_groups = null;
    },
    RESET_SOCIAL_GROUPS(state) {
        state.social_groups = null;
    },
    RESET_RESIDENCE_LOCATIONS(state) {
        state.residence_locations = null;
    },
    RESET_POPULATION(state) {
        state.population = null;
    },
    RESET_ALL_DATA(state) {
        state.activities   = null;
        state.city         = null;
        state.country      = null;
        state.document     = null;
        state.eps          = null;
        state.location     = null;
        state.supercade    = null;
    }
};

export default mutations;