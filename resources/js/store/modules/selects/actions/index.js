const actions = {
    set_initial_data_selects: ({commit}, response) => {
        commit('INITIAL_DATA', response);
    },
    set_document_types: ({commit}, response) => {
        commit('SET_DOCUMENT_TYPES', response)
    },
    set_sex: ({commit}, response) => {
        commit('SET_SEX', response)
    },
    set_genders: ({commit}, response) => {
        commit('SET_GENDERS', response)
    },
    set_ethnic_groups: ({commit}, response) => {
        commit('SET_ETHNIC_GROUPS', response)
    },
    set_social_groups: ({commit}, response) => {
        commit('SET_SOCIAL_GROUPS', response)
    },
    set_residence_locations: ({commit}, response) => {
        commit('SET_RESIDENCE_LOCATIONS', response)
    },
    set_population: ({commit}, response) => {
        commit('SET_POPULATION', response)
    },

    reset_document_types: ({commit}) => {
        commit('RESET_DOCUMENT_TYPES');
    },
    reset_sex: ({commit}) => {
        commit('RESET_SEX');
    },
    reset_genders: ({commit}) => {
        commit('RESET_GENDERS');
    },
    reset_ethnic_groups: ({commit}) => {
        commit('RESET_ETHNIC_GROUPS');
    },
    reset_social_groups: ({commit}) => {
        commit('RESET_SOCIAL_GROUPS');
    },
    reset_residence_locations: ({commit}) => {
        commit('RESET_RESIDENCE_LOCATIONS');
    },
    reset_population: ({commit}) => {
        commit('RESET_POPULATION');
    },
    reset_init_data: ({commit}) => {
        commit('RESET_ALL_DATA');
    }
};

export default actions;