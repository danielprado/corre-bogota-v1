import actions from '@/store/modules/selects/actions';
import getters from '@/store/modules/selects/getters';
import mutations from '@/store/modules/selects/mutations';

const state = {
    document_types: null,
    sex: null,
    genders: null,
    ethnic_groups: null,
    social_groups: null,
    residence_locations: null,
    population: null,
};

export default { actions, mutations, getters, state }

