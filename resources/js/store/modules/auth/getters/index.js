const getters = {
    getUsername: state => ( state.username ) ? state.username : null,
    getPermissions: state => ( state.permissions ) ? state.permissions : window.permissions,
    getAuth: state => ( state.auth ) ? state.auth : null,
};

export default getters;