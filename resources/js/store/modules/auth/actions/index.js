import {Api} from "@/models/Api";

const actions = {
    login: ({commit}) => {
        return new Promise((resolve, reject) => {
            window.axios.get( Api.END_POINTS.GET_PROFILE() )
                .then( (response) => {
                    commit('LOGIN', response.data);
                    resolve( response.data );
                })
                .catch((error) => {
                    reject( error.response.data );
                });
        })
    },
    logout: ({commit}) => {
        return new Promise((resolve, reject) => {
            window.axios.post( Api.END_POINTS.LOGOUT() )
                .then((response) => {
                    commit('LOGOUT');
                    resolve( response );
                })
                .catch(() => {
                    window.location = `${process.env.MIX_IDRD_URL}/logout`;
                });
        });
    },
};

export default actions;