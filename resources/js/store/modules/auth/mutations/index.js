import Cookies from 'js-cookie';
import {Api} from "@/models/Api";

const mutations = {
    LOGIN( state, response ) {
        state.username = response.data.username;
        state.auth = response.data.auth;
        window.permissions = response.data.permissions;
        state.permissions = response.data.permissions;
    },
    LOGOUT(state) {
        state.username = null;
        state.auth = null;
        state.permissions = null;
        window.permissions = null;
        window.isAuthenticated = false;
        Cookies.remove( Api.COOKIE );
        window.sessionStorage.clear();
    },
};

export default mutations;