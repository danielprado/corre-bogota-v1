import actions from '@/store/modules/counters/actions';
import getters from '@/store/modules/counters/getters';
import mutations from '@/store/modules/counters/mutations';

const state = {
    counter_people: 0,
    counter_sessions: 0,
    counter_week: 0,
    counter_month: 0,
};

export default { actions, mutations, getters, state }

