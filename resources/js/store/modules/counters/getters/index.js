const getters = {
    getTotalPeople: state => ( state.counter_people ) ? state.counter_people : null,
    getTotalSessions: state => ( state.counter_sessions ) ? state.counter_sessions : null,
    getTotalWeek: state => ( state.counter_week ) ? state.counter_week : null,
    getTotalMonthl: state => ( state.counter_month ) ? state.counter_month : null,
};

export default getters;