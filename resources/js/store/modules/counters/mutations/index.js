const mutations = {
    COUNTERS( state, response ) {
        state.counter_people   = response.data.counter_people;
        state.counter_sessions   = response.data.counter_sessions;
        state.counter_week   = response.data.counter_week;
        state.counter_month   = response.data.counter_month;
    },
    RESET(state) {
        state.counter_people = null;
        state.counter_sessions = null;
        state.counter_week = null;
        state.counter_month = null;
    },
};

export default mutations;