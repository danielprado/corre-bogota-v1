const actions = {
    counters: ({commit}, response) => {
        return new Promise((resolve, reject) => {
            commit('COUNTERS', response);
            resolve();
        })
    },
    reset_counter: ({commit}) => {
        return new Promise((resolve, reject) => {
            commit('RESET');
            resolve();
        })
    }
};

export default actions;