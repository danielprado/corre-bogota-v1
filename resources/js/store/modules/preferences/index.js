import actions from '@/store/modules/preferences/actions';
import getters from '@/store/modules/preferences/getters';
import mutations from '@/store/modules/preferences/mutations';

const state = {
    accent: 'purple',
    background_color: 'black',
    mini: false,
    image: true,
    background_image: `${process.env.MIX_IDRD_URL}/public/img/sidebar-2.jpg`,
    lang: 'es',
    is_RTL: false
};

export default { actions, mutations, getters, state }

