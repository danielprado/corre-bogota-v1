const getters = {
    getAccent: (state) => state.accent,
    getBackgroundColor: (state) => state.background_color,
    getMini: (state) => state.mini,
    getImageStatus: (state) => state.image,
    getLang: (state) => state.lang,
    getBackgroundImage: (state) => state.background_image,
    getIsRTL: (state) => state.is_RTL,
};

export default getters;