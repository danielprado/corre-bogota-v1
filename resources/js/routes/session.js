import Vue from "vue";
import Auth from "@/package/auth"
Vue.use(Auth);
import DashboardLayout from "@/pages/Dashboard/Layout/DashboardLayout.vue";
import Sessions from "@/pages/Dashboard/Session/Session";
import EditSessions from "@/pages/Dashboard/Session/EditSession";
import SessionTable from "@/pages/Dashboard/Session/SessionTable";
import Attendance from "@/pages/Dashboard/Session/Attendance"
const session = {
  path: "/sessions",
  component: DashboardLayout,
  redirect: "/participants/my-sessions",
  name: "Sessions",
  children: [
    {
      path: "my-sessions",
      name: "My Sessions",
      component: SessionTable,
      meta: {
        requiresAuth: true,
        can: Vue.auth.can('access_platform'),
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: "create",
      name: "Create Session",
      component: Sessions,
      meta: {
        requiresAuth: true,
        can: Vue.auth.can('access_platform'),
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: ":id/edit",
      name: "Edit Session",
      component: EditSessions,
      props: true,
      meta: {
        requiresAuth: true,
        can: Vue.auth.can('access_platform'),
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: ":id/attendance",
      name: "Attendance",
      component: Attendance,
      props: true,
      meta: {
        requiresAuth: true,
        can: Vue.auth.can('access_platform'),
        //rtlActive: Vue.auth.isRTL
      }
    }
  ]
};

export default session;