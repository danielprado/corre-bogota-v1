import Vue from "vue";
import Auth from "@/package/auth"
Vue.use(Auth);
import DashboardLayout from "@/pages/Dashboard/Layout/DashboardLayout.vue";

import Participant from "@/pages/Dashboard/Participant/Participant.vue";
import EditParticipant from "@/pages/Dashboard/Participant/EditParticipant.vue";
import ParticipantTable from "@/pages/Dashboard/Participant/ParticipantsTable.vue";

const running = {
  path: "/participants",
  component: DashboardLayout,
  redirect: "/participants/registered",
  name: "Participants",
  children: [
    {
      path: "registered",
      name: "Registered",
      component: ParticipantTable,
      meta: {
        requiresAuth: true,
        can: Vue.auth.can('access_platform'),
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: "create",
      name: "Create Participant",
      component: Participant ,
      meta: {
        requiresAuth: true,
        can: Vue.auth.can('access_platform'),
        //rtlActive: Vue.auth.isRTL
      }
    },
    {
      path: ":id/edit",
      name: "Edit Participant",
      component:  EditParticipant ,
      props: true,
      meta: {
        requiresAuth: true,
        can: Vue.auth.can('access_platform'),
        //rtlActive: Vue.auth.isRTL
      }
    }
  ]
};

export default running;
