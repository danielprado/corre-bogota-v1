import Vue from "vue";
import Auth from "@/package/auth"
Vue.use(Auth);
import DashboardLayout from "@/pages/Dashboard/Layout/DashboardLayout.vue";
import Evidences from "@/pages/Dashboard/Evidences/Evidence";


const files = {
  path: "/files",
  component: DashboardLayout,
  redirect: "/files/evidences",
  name: "Files",
  children: [
    {
      path: "evidences",
      name: "Evidences",
      component: Evidences,
      meta: {
        requiresAuth: true,
        can: Vue.auth.can('access_platform'),
        //rtlActive: Vue.auth.isRTL
      }
    }
  ]
};

export default files;