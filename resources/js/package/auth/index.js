import store from "@/store"

export default function (Vue) {
    Vue.auth = {
        username: () => store.getters.getUsername,
        auth: () => store.getters.getAuth,
        isRTL: () => store.getters.getIsRTL,
        isAuthenticated: () => window.isAuthenticated,
        can: function ( permissions ) {
            let perms = window.permissions || store.getters.getPermissions;
            if ( perms ) {
                if (typeof permissions === 'string') {
                    return  perms.hasOwnProperty(permissions) && perms[ permissions ] === 1;
                }
                let some = permissions.map( (p) => {
                    return perms.hasOwnProperty( p ) && perms[ p ] === 1;
                });
                return some.includes(true);
            }
            return false;
        },
        destroySession: () => store.dispatch('logout')
    };
    Object.defineProperties(Vue.prototype, {
        $auth: {
            get () { return Vue.auth }
        }
    });
}