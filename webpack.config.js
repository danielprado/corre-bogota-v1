const SWPrecacheWebpackPlugin = require("sw-precache-webpack-plugin");

module.exports = {
  resolve: {
    extensions: [".js", ".vue", ".json"],
    alias: {
      vue$: "vue/dist/vue.esm.js",
      "@": __dirname + "/resources/js"
    }
  },
  output: {
    chunkFilename: "js/chunk_[name].bundle.js",
    publicPath:
      process.env.NODE_ENV === "development"
        ? "/public/"
        : `/SIM/corre-bogota-v1/public/`
  },
  plugins: [
    new SWPrecacheWebpackPlugin({
      cacheId: "idrd_passport",
      filename: "service-worker.js",
      staticFileGlobs: [
        "public/**/*.{js,html,css,png,gif,jpg,eot,ttf,ico,xml,json,txt,svg}"
      ],
      minify: true,
      stripPrefix: "public/",
      runtimeCaching: [
        {
          urlPattern: /^https:\/\/fonts\.googleapis\.com\//,
          handler: "cacheFirst"
        },
        {
          urlPattern: /^https:\/\/ajax\.googleapis\.com\//,
          handler: "cacheFirst"
        },
        {
          urlPattern: /^https:\/\/use\.fontawesome\.com\//,
          handler: "cacheFirst"
        }
      ]
    })
  ]
};
